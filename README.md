Website
=======

このレポジトリは https://www.clear-code.com/ の内容を管理します。

| #            | URL                         |
| ------------ | --------------------------- |
| 本番サイト   | https://www.clear-code.com  |
| ステージング | https://clear-code.gitlab.io/website |
| GitLab       | https://gitlab.com/clear-code/website |

開発情報
--------

### ローカルビルドの構築方法

```bash
$ git clone https://gitlab.com/clear-code/website.git
$ cd website
$ bundle install --path .b
$ bundle exec rake build
```

ビルド済みのコンテンツは`_site`配下に出力されます。

### ステージング環境への反映

ブランチを切って、GitLabにプッシュすると、自動的に内容がステージング環境に反映されます。

* 例えば`test`ブランチをプッシュすると https://clear-code.gitlab.io/website/test に公開されます。

### 本番環境への反映

GitLabに`master`ブランチにプッシュすると、自動的に www.clear-code.com にデプロイされます。

個別ページの管理
----------------

### プレスリリースの更新

1. `YYYYMMDD-title${拡張子}`というファイル名でプレスリリースを起稿します。`${拡張子}`は`.md`推奨です。（`.html`も可）
2. 作成したファイルを`/press-releases/`に追加します。
3. `_data/services.yaml`にエントリを追加します。2021年8月現在は、リンク先のURLの拡張子は`.html`になります。`.md`でプレスリリースを作っても最終的に`.html`に変換するためです。

実際の例: 452f86cee

### トピックの更新

1. `YYYYMMDD-title${拡張子}`というファイル名でトピックを起稿します。`${拡張子}`は`.md`推奨です。（`.html`も可）
2. 作成したファイルを`/topics/`に追加します。
3. `_data/topics.yaml`にエントリを追加します。2021年8月現在は、リンク先のURLの拡張子は`.html`になります。`.md`でプレスリリースを作っても最終的に`.html`に変換するためです。

実際の例: 018b7ee65

### ククログの投稿

詳細は[ククログの記事の書き方](https://www.clear-code.com/blog/2021/5/28/how-to-write-article.html)を参照。

1. `{title}.md`というファイルを`_drafts`ディレクトリ配下に作成し、ブログ記事を書きます。
    * publish時に日付プレフィックスが自動付与されるので、この時点では付けなくて良いです。
1. `_config.yaml`に（まだ未登録であれば）作者やタグの定義を追加します。
    * タグを新規に追加する場合は、`/blog/{tag}/index.html`ファイルも作成します。タグをクリックしたときのページに必要です。
1. MRを作成し、レビューをもらったら、publishをしてもらいます。
    * マージする人が`rake drafts:publish`を実行します。

注意点

* ククログ記事へのリンク: `{% post_url (_posts配下のファイル名、拡張子無し) %}`
    * 例: `[クリアコードとフリーソフトウェアとビジネス]({% post_url 2015-09-17-index %})`
* クログ記事以外のサイト内のページへのリンク: `{% link (ファイルパス) %}`
    * こちらは拡張子も付ける
    * 先頭に`/`を付けないように注意
    * 例: `[サービス]({% link services/index.md %})`
* 脚注の識別子に`w`を使えないバグがあるため、`uu`などで代替する
    * [すでに修正はされている](https://github.com/github/cmark-gfm/pull/229)が、[jekyll-commonmark-ghpages](https://github.com/github/jekyll-commonmark-ghpages)の依存関係の問題で取り込めていない
    * [upstreamでお願いしている](https://github.com/github/jekyll-commonmark-ghpages/issues/22)

実際の例

* 9d1d54785
* 41d1a45a (タグを新規追加)

### ククログのpublish

レビューが終わったら、レビューアーがククログをpublishします。

次の2つの方法があります。

#### マージリクエスト上でpublishする

1. マージリクエスト用ブランチ上で、記事を公開するための変更を行う。
   1. `git fetch "git@gitlab.com:(執筆者のアカウント)/website.git" (ブランチ名) && git checkout -b "(執筆者のアカウント)/website-(ブランチ名)" FETCH_HEAD` でマージリクエストの内容を手元に持ってくる。
   2. `rake drafts:publish` で `_drafts/` 配下にあるレビュー済みの記事本文のファイルを `_posts/` 配下に移動して該当ブランチを更新する。
2. マージリクエストをマージする。
3. 記事が公開される。

#### マージリクエストをマージした後でpublishする

1. マージリクエストをマージする。
1. 自身のローカルをpullなどで更新して最新のmasterブランチをチェックアウトする。
1. `rake drafts:publish` で `_drafts/` 配下にあるレビュー済みの記事本文のファイルを `_posts/` 配下に移動してmasterを更新する。
1. 記事が公開される。

### プレビューURL自動コメント機能の設定

* 自身のforkリポジトリにコミットすると、 `https://{username}.gitlab.io/website/` からプレビューを確認できる
  * daipomの例: https://daipom.gitlab.io/website/
* プレビューを確認するにあたり `https://{username}.gitlab.io/website/pages` にて `Use unique domain` のチェックを外す
  * チェックが入っているとunique domainにリダイレクトされCSSが参照できないため
* 各ブランチ用のプレビューのリンクがあるので、選択してブログを確認する

MRに自動でプレビューURLをコメントさせるためには次の設定をする。

1. personal access tokenを作る
    * https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token
    * `Edit profile -> Access Tokens`の設定
    * 各設定値
        * Token name: `PREVIEW_URL_TOKEN` (何でも良い)
        * Expiration date: 期限なし
        * Select scope: `api`のみ
    * 設定すると、`Your new personal access token`という欄にトークンが表示されるので、コピーする
        * このときしか表示されないので注意
1. `PREVIEW_URL_TOKEN`という名前のGitLab CI用の変数を作成し、上のトークンをセットする
    * https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project
    * forkリポジトリの`Settings -> CI/CD`ページの`Variables`の設定
    * 各設定値
        * Protect variable: チェックなし
        * Mask variable: チェックあり
        * Key: `PREVIEW_URL_TOKEN`
        * Value: コピーしたトークン

画像の保存
----------------

### いろいろなところで使用する画像の登録

1. `/images/`に画像を登録。

### 記事に紐づく画像の登録
1. 特定記事と同名のファルダを記事の入っている場所に作成。
2. フォルダ内に画像を登録します。

例：　
```
press-release
├── 20210113-redmine.html
├── 20210113-redmine
│   └── redmine.png
├── 20210325-system-admin-girl.html
├── 20210325
│   └── bookcover.jpg
...
```
### 画像のライセンスに関する注意事項
クリアコードが著作者ではない画像あるいはクリアコードがデフォルトで設定しているライセンスでないコンテンツについては、[ライセンスページ](license/index.html)にも情報を追記するようにしてください。

### サイトからリンクを貼るときの注意事項
* サイト内のページにリンクする場合は、ステージング環境でリンクが自動的に解決されるよう、`{% link ～ %}` を使って `[リンクテキスト]({% link path/to/file %})` のようにします。（たとえば、[サービスのトップページ]({% link services/index.md %})であれば `[サービス]({% link services/index.md %})` です。）
  * **2021年11月9日時点の注記：** ただし、[ククログのトップページ]({{ "blog/" | relative_url }})だけはこの方法でリンクするとエラーになります。代替として、 `[リンクテキスト]({{ "blog/" | relative_url }})` と書く必要があります。関連していそうなissueとpull request：[sverrirs/jekyll-paginate-v2#104](https://github.com/sverrirs/jekyll-paginate-v2/issues/104)/[sverrirs/jekyll-paginate-v2#226](https://github.com/sverrirs/jekyll-paginate-v2/pull/226)
