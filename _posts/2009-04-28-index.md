---
tags:
- mozilla
title: Fennecでの日本語表示設定
---
### Nightlyビルドでの日本語対応状況

[2月26日のエントリ]({% post_url 2009-02-26-index %})のエントリでお伝えした通り、以前のWindows Mobile版Fennecには日本語表示に致命的なバグが存在していましたが、弊社エンジニアがこれを修正し、本家にも既にこの修正が取り込まれております。このため、現在mozilla.orgで公開されているNightlyビルドでも、バイナリを修正することなく日本語を表示することが可能です。
ただし、現在のバージョンではまだフォントの自動選択処理に不具合があるため、設定を正しく行っていない場合、インストールしたフォントや訪れたサイトによっては、文書の一部あるいは全てが文字化けする可能性があります。
そこで今回は、4/28現在のNigtlyビルドにおいて日本語を正しく表示する設定を紹介致します。
<!--more-->


### Nightlyビルドのインストール

[mozilla.orgのFTPサイト](ftp://ftp.mozilla.org/pub/mobile/nightly/)から最新のcabファイルを取得し、インストールします。その後、一度Fennecを起動して、プロファイルを作成します。プロファイルは \Application Data\Mozilla\Fennec\Profiles\xxxxxxxx.default フォルダ以下に作成されます。この後、設定ファイルを手動で変更しますので、一旦Fennecを終了します。

### 日本語フォントのインストール

現在のFennecは、Windows Mobile日本語版に標準で搭載されているAC3形式のフォントに対応していません。このため、別途日本語TTFフォントをインストールする必要があります。ここでは例として、VLゴシックをインストールします。

  * [VLゴシックのサイト](http://dicey.org/vlgothic/)からzipアーカイブを取得
  * 上記zipアーカイブからVL-PGothic-Regular.ttfを抽出
  * 上記TTFファイルを \windows フォルダにコピー

### prefs.jsの設定

プロファイルフォルダ以下のprefs.jsファイルを開いて、以下ような記述を追加します。

{% raw %}
```
user_pref("font.language.group", "ja");
user_pref("font.name.sans-serif.ja", "VL PGothic");
user_pref("font.name.sans-serif.x-unicode", "VL PGothic");
user_pref("font.name.sans-serif.x-western", "VL PGothic");
user_pref("font.name.serif.ja", "VL PGothic");
user_pref("font.name.serif.x-unicode", "VL PGothic");
user_pref("font.name.serif.x-western", "VL PGothic");
user_pref("intl.accept_languages", "ja");
user_pref("intl.charset.default", "UTF-8");
user_pref("intl.charset.detector", "ja_parallel_state_machine");
```
{% endraw %}

TrueTypeフォントであれば、VLゴシックに限らずどのフォントでも使用可能と思われます。例えばIPA Pゴシックを使用する場合は、VL PGothicという記述をIPAPGothicに変更します。

### userContent.cssの設定

以上でほぼ日本語の表示は可能なのですが、これでもなおフォーム内の文字が化ける場合があります。今回はユーザーCSSでフォントを指定して、この問題を回避します。
プロファイルフォルダ以下に chrome というフォルダを作成し、userContent.css という名前で以下のような内容のファイルを作成します。

{% raw %}
```
input[name], input[value], select[name], option, textarea, button, fieldset, label, legend, optgroup[label] {
  font-family: 'VL PGothic', sans-serif;
  font-size: 12px;
}
```
{% endraw %}

以上で、フォームでも日本語が表示されるようになります。

### まとめ

Fennecで日本語を表示することはできたでしょうか? クリアコードでは、上記のような煩わしい設定を行わなくともFennecで日本語を表示できるよう、引き続き改善作業を行っています。また、現在のNightlyビルドにはまだ含まれていないものの、日本語入力時の未確定文字列が表示されない問題等についても修正を行い、本家にフィードバックしています。
