---
title: "RubyKaigi 2022 after event for Fast data processing with Ruby and Apache Arrow #rubykaigi"
author: fujimotos
tags:
  - apache-arrow
  - ruby
---


この記事は9月22日にYouTube Liveで放送した [RubyKaigi 2022 after event for Fast data processing with Ruby and Apache Arrow](https://www.youtube.com/watch?v=bpuJWC9_USY) のまとめです。

Apache ArrowのPMC chairである須藤が話し手、たなべさんを聞き手として、Apache Arrowに関する質問に回答をするという趣向の放送でした。

<!--more-->

### Q1. Red Arrowの使い方について

> Red Arrowは開発者向けの低レイヤーの機能を備えていると理解していますが、
> データ解析をしようとするユーザーから見ると多機能／高機能過ぎてとっつきにくい面があるように思います。
>
> 例えば、Chunked Arrayは大きなデータを取り扱う際には必要だろうと想像していますが、ユーザーレベルでは意識しないで使えるようになってほしいのではないでしょうか。
> このあたり他の言語ではレイヤーの棲み分けみたいものがあるかとか、Rubyではどうあるべきかお考えを教えていただけると嬉しいです。

#### Red Arrowとは何か？

* Apache Arrowとは何か？ 一言でいえば「メモリに乗り切らないデータをインメモリで処理するライブラリ」である。
  このためにChunked Arrayという技術を利用する。これは巨大なデータを小さな配列（チャンク）に分割して保持するテクニックである。
* Red ArrowはApache ArrowのRubyバインディングである。C++のライブラリの広範な機能をRubyから呼び出せるようにする。
  ただ、元のライブラリの機能が非常に多いので、今回の質問につながっているのだと思う。

#### 他の言語の状況について

* 他の言語だと、実は低レベルな機能は意識せずに使えるようになっている。 例えば、PythonだとArrow形式のデータを[pandas](https://pandas.pydata.org/)のデータフレームに（可能であればゼロコピーで）変換できる。
* また、Rはより統合が進んでいて、[dplyr](https://dplyr.tidyverse.org/)のバックエンドとして使える。Red Arrowでも大きくこういった使いやすいAPIを提供する方向性を目指している。

#### Red Arrowが提供する機能について

* Ruby向けの実装として、まず [heronshoes](https://github.com/heronshoes) さんが開発している[RedAmber](https://github.com/heronshoes/red_amber)がある。
  これはpandasやdplyrのような機能を提供するもので、Red Arrowをバックエンドとして使っている。（配信では複数のバックエンドを使えるというような説明をしていたがそれは嘘情報だった。）
* もう一つ、検討を進めているものとして[red-table-query](https://github.com/red-data-tools/red-table-query)がある。
  これはActive Recordが統一的なDBアクセスを可能にするように、表形式データを統一的に扱えるようにするプロジェクト。
* 参考にしている技術として、[jelemyevans](https://github.com/jeremyevans/) さんが開発している[Sequel](http://sequel.jeremyevans.net/)がある。このインターフェイスは実によくできていて感心する [^seq]
* red-table-queryはC#のLINQのようなインターフェイスを提供することや、Perl/PythonのDBIのようにバックエンドによらない統一的なインターフェイスになることを目指している。ただ、まだ具体的な実装はこれからのステータス。

[^seq]: 例えば`dataset.where({(value >=50) & (value <= 100)})`とフィルタできたり、`DB[:items].select{avg(price) + 100}`として値を集約できたりする。

### Q2. Red Arrowの実装レイヤーについて

> Red Arrowは　C++、C GLib、GObject Introdspection、Rubyといった感じで実装上のレイヤーがあると思うのですが、
> Rubyしか読めない私にとってはどの機能がどこまでカバーされているのか良くわからないことがあります。
> ざっくり、追随の状態とか、ここ調べたらわかるよ的な情報をいただけると嬉しいです。

#### Red Arrowの階層構造について

* 全体像としては`Red Arrow →  Apache Arror C GLib →  Apache Arrow C++ `という階層になっている。
  GLibを利用するのにRuby/GObjectIntrospectionを使っているが、この部分の詳細は過去の発表 [^2016] を見てほしい。
* 基本的にはApache Arrow C++にすべての機能があり、Apache Arrow C GLibはCライクな便利APIを提供している。
  Ruby/GObjectIntrospectionはインターフェイスを機械生成するもので、Red Arrowはその上に便利なAPIを提供している。
* ただ高機能な`Arrow::Table.load`/`Arrow::Table#save`のようなメソッドはRubyレベルで実装している。便利なAPIの実装については、Rubyから調べていくとよい。

[^2016]: [RubyKaigi 2016: How to Create Bindings 2016]({% post_url 2016-09-14-index %})

#### Apache ArrowとRed Arrowの対応について

* Apache Arrow C GLibは実質的に自分しか書いておらず、少し悲しい。
* 最近の機能はまだ追従できてない。特に、パーティショニング機能（複数ファイルに小分けにする機能）は入れたいが、GLibにはまだ入れられてない。
  やりたい人がいれば手を上げてほしい。

### Q3. Apache Arrow DatasetとHDF5の比較について

> Apache Arrow Datasetは複数のデータソースをひとまとめにして扱う方法を提供するものだと以前教えていただきました。
> 物理分野でよく使われるHDF5の用途に関係するもののようにとらえていますが、どんな特徴がありますか？

#### Apache Arrow Datasetとは

* Apache Arrow Datasetは複数のデータソースを統一的に扱えるライブラリーであっている。複数のデータソースを同じAPIで扱えるし、複数のデータソースを混ぜて論理的に一つのデータソースのように扱うこともできる。
* HDF5には余り詳しくないが、ストリーム処理ができるかどうかが大きな違いだと思う。Apache Arrow Datasetはストリーム処理できる。また、Apache Arrow DatasetはローカルファイルだけでなくHDFS・S3・GCS・Auzure Blob Storageなどのリモートのストレージも同じAPIで扱える。

### 付録: 放送時に描いた解説図

![YouTube Live放送時の解説図]({% link images/blog/20220930/rubykaigi-2022-after-event-2022-09-22.svg %})

### おわりに

今回の放送はここまでで終わりです。説明が白熱したこともあって、
質問を全て消化しきらないうちに放送時間を使い切ってしまったので、今回の放送の続編を2022年10月21日に開催します。

  * イベント名：第2回 RubyKaigi 2022 after event - 「Fast data processing with Ruby and Apache Arrow」のスピーカーに質問しよう！
  * 目的：RubyKaigi 2022のトーク「Fast data processing with Ruby and Apache Arrow」で話されなかったことについてスピーカーに質問して理解を深めよう！
  * 日時：10月21日（金）10:00-11:00
  * 場所：オンライン（YouTube live: https://www.youtube.com/watch?v=TAnUgMcQgl8 ）
  * 参加者：
    * 須藤（スピーカー）
    * 田辺（聞き手、[@sunaot](https://twitter.com/sunaot)、[RubyKaigi 2022でシャトルバススポンサーをしていたエス・エム・エスさん](https://tech.bm-sms.co.jp/entry/2022/09/11/182627)の技術責任者）
  * 参加者以外からの質問も受け付けるので、聞きたいことがある人はイベント終了までの間に次のいずれかの手段で質問を共有してください。
    * [↑のYouTube liveページ](https://www.youtube.com/watch?v=TAnUgMcQgl8)のコメント・チャット
    * [Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)
    * Twitterで[#reddatatools](https://twitter.com/hashtag/reddatatools)付きでツイート

放送に関する感想などは [@\_clear\_code](https://twitter.com/_clear_code) 宛にお寄せください。
