---
tags: []
title: OSC2014 Tokyo/FallでMySQLユーザー向けに日本語全文検索について紹介予定
---
今週末（2014年10月18日（土）と19日（日））、[オープンソースカンファレンス2014 Tokyo/Fall](https://www.ospn.jp/osc2014-fall/)が開催されます。初日（18日（土））の11:00から11:45の枠で[日本MySQLユーザ会](http://www.mysql.gr.jp/)の須藤が[いろいろ考えると日本語の全文検索もMySQLがいいね！](https://www.ospn.jp/osc2014-fall/modules/eguide/event.php?eid=6)という発表をします。
<!--more-->


次の人を対象とした内容です。

  * MySQLユーザー
  * 全文検索に関する知識はない（あるいは、ほとんどない）
  * でも、日本語の全文検索を実現することになってしまった（あるいは、実現したいと常々思っている）

MySQLを使っていて、「日本語の全文検索を実現したいけどWebの情報を見るとひと手間必要そうで敷居が高いなぁ…」という人は、ぜひ18日（土）に[明星大学 日野キャンパス](http://www.meisei-u.ac.jp/access/hino.html)[^0]にお越しください。

全文検索についての知識がない人向けの内容なので、全文検索のあまり細かいことまでは触れない予定です。その代わり、全文検索の知識がなかった人には、「なるほどねー」とわかった気になれるような内容を目指します。

全文検索の細かいことが知りたい人はMySQLユーザー会のブース[^1]に来て聞いてください。須藤と[@yoku0825](https://twitter.com/yoku0825)がブース番をしている予定です。須藤は[Groonga](http://groonga.org/ja/)や[Mroonga](http://mroonga.org/ja/)といった全文検索関連プロダクトの開発に関わっているので詳しく説明することができます。MySQLそのものについては[Oracle ACEの@yoku0825](http://yoku0825.blogspot.jp/2014/04/mysql-at-mynamysql-20144.html)が詳しく説明することができます。ちなみに、隣のブースはOracle Corporation (MySQL Community Team)ということなので、MySQLに関することは一緒に力を合わせて説明できます。全文検索に関係ないことでもMySQLに関することを知りたい方はぜひブースにお立ち寄りください。

発表内容が具体的にどんな内容になるか事前に知りたい人は[GitHubのリポジトリー](https://github.com/kou/rabbit-slide-kou-osc-2014-tokyo-fall)をのぞいてみてください。発表資料はここで随時アップデートされています。

MySQLで日本語全文検索をしたい人の助けになる内容を目指しますので、MySQLユーザーで日本語全文検索に興味のある方はぜひお越しください。お待ちしています！

以下は参考URLのリストです。

  * [オープンソースカンファレンス2014 Tokyo/Fallの開催概要](https://www.ospn.jp/osc2014-fall/)
  * [会場へのアクセス方法](http://www.meisei-u.ac.jp/access/hino.html)
  * [「いろいろ考えると日本語の全文検索もMySQLがいいね！」のセミナーページ](https://www.ospn.jp/osc2014-fall/modules/eguide/event.php?eid=6)
  * [「～MySQL Central 2014で発表された最新情報をフィードバック～」のセミナーページ](https://www.ospn.jp/osc2014-fall/modules/eguide/event.php?eid=46)
    * MySQLユーザ会だけでなく、Oracle Corporation (MySQL Community Team)もセミナー枠があります。「9/28(日)～10/2(木)にサンフランシスコで開催された"MySQL Central"で発表された最新情報」を紹介してくれるようです。MySQLに興味がある方はあわせてこちらも参加してみてはいかがでしょうか。

[^0]: OSC2014 Tokyo/Fallの会場です。新宿から1時間くらいの場所にあります。

[^1]: 土曜日のみ出展です。日曜日は出店していません。
