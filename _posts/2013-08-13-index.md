---
tags:
- internship
- test
title: インターンシップで学んだこと2：1人で開発しているときはていねいに開発を進める
---
[インターンシップ1日目と2日目ではコメントに注目するとよい]({% post_url 2013-08-05-index %})ということを学びました。[インターンシップ3日目のメモ](https://github.com/clear-code/internship/blob/master/2/mentor/3.md)を読み返すと3日目は3つ学んだことがありました。今回はそのうちの1つ「1人で開発しているときにtypoとどうつきあっていくか」です。
<!--more-->


### 背景

インターンシップは基本的にメンターがいつもインターンのそばにいて一緒に開発を進めていく予定でしたが、打ち合わせなど他の予定が入ってしまうこともありました。そのようなときはインターンが1人で開発を進めていました。メンターは他の予定から戻ってきたときに「どうだった？」「詰まっているところはない？」などと聞きながらコミットメールを読んでいました。

コミットメールを読めばだいたいどんなことをやっていたかはわかりますが、「変なことをしていないか？」という視点で読んでいるわけではないので細かいミスすべてに気づくわけではありません。どちらかというと[時間を越えてペアプログラミングしている気分]({% post_url 2012-03-13-index %})で読んでいます。これは、コードレビューでの読み方とは違う読み方です。

そんなわけで、2日目に追加した[テスト名にtypoがあり](https://github.com/ranguba/epub-searcher/commit/b47c150b664d4c7e52a827107c183ac5446026f9)、テストとして認識されていないことに気づいていませんでした[^0]。typoを直してテストとして認識されるようにしたところ[テストが失敗](https://travis-ci.org/ranguba/epub-searcher/builds/8223605)していました。せっかくテストを追加しながら開発していたのですが活かせていませんでした。

typoはよくあることですし、すぐに直せることなので、typoしてしまうことが悪いことだとは思いません。typoを防ぐことよりもtypoしてもすぐに気づいて直せるようにした方が現実的でしょう。ということで、「1人で開発しているときにtypoとどうつきあっていくか」についてです。どうつきあっていくかを最初に言うと、ていねいに開発を進めてつきあっていく、です。

### 開発を継続するためのテスト

開発するときはテストを書きますが、それは開発を駆動するためではないのでテスト駆動開発という言葉を使うことはあまりありません。「テストを書きながら開発をするやり方」をざっくりと説明するために便利なのでテスト駆動開発という言葉を使うくらいです。テストを書くのは開発を駆動するためではなく、開発を継続するためです。

いくつもフリーソフトウェアの開発に関わっていると、数カ月ぶりにコミットするということがあります。そんなときはコードの隅々まで覚えているという状態ではありません。そんな状態でも安心してコードを変更できるようにするためにテストを書いておきます。他にも、開発を長く続けていると新しいプラットフォームや新しいライブラリーに対応したりする場合にテストがあると安心してコードを変更できます。つまり、いつまでも安心してコードを変更していけるように、開発を継続していけるように、そのためにテストを書いています。

開発を駆動するためにテストを書いているわけではないので、テストファーストで開発を進めるときもあればそうでない場合もあります。入力のパターンがわかっている場合は1つずつテストファーストで開発を進めていきますが、さぐりさぐりで開発しているときはある程度実装が見えてきてからテストを書きます。

このように、あまりテスト駆動開発を取り入れていませんが、テスト駆動開発の中にていねいに開発を進めるためによいやり方があるので、それは取り入れています。

### 最初にテストを失敗させる

テスト駆動開発の中にあるていねいに開発を進めるためのよいやり方とは「最初にテストが失敗させ、ちゃんとテストが実行されていることを確認する」やり方です。これは、1人で開発しているときでもなるべく早くtypoに気づけるようになるからです。具体的にどうやっているかというと、最初は期待する値として`"XXX"`という必ず失敗する値を指定して実行しています。別に`"XXX"`でなくてもよいのですが、コードのコメントの中で「なんか気をつけて！」くらいの意味合いで「XXX」が使われるので、失敗する値としても`"XXX"`を使っています。

{% raw %}
```ruby
def test_plus
  assert_equal("XXX", 1 + 2)
end
```
{% endraw %}

このコードをコミットすることはないのでこのように開発していることを知っている人はほとんどいないでしょう。インターンシップのときも伝え忘れました。単に、「テストを書いたら最初に失敗させてテストが実行されていて確認するといいよ」と伝えたくらいだった気がします。

### テストでは補完機能を使わない

typoを早く見つけるために気をつけていることがもう1つありました。それは、テストコードの方ではあまりエディターの補完機能を使わないということです。テストコード固有のところでは使いますが、テスト対象のコードを書くとき（開発した機能をテストの中で使っているとき）には使いません。これは、補完機能を使ってしまうと開発しているコードの中にあるtypoをそのままテストでも使ってしまうからです。そのまま使ってしまうとtypoに気づくことができません。

テストコードの中では開発しているコードを使う側の気持ちになって書きたいので、エディターの補完機能に頼らず実際に入力しています。

このやり方もインターンシップ中には伝え忘れた気がします。

### まとめ

メインで開発している人が自分だけの場合は、typoに気づきづらいものですが、ていねいに開発を進めれば早いうちに気づけるものですよ、ということをまとめました。普段、意識せずにやっていて、インターンシップのときもそれほどまとまった形で伝えられなかったことが、こうしてインターンシップのメモをまとめ直すことでまとまった形にできてよかったです。

[^0]: こういうときは`def test_XXX`ではなく、`test "..." &#123;&#125;`でテストを定義したほうがよいのではないかという気がしてきます。
