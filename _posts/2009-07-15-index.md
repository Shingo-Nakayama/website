---
tags:
- ruby
- milter-manager
title: 日本Ruby会議2009でのリリース予定
---
せっかくの機会なので、[日本Ruby会議2009](http://rubykaigi.org/2009/ja/)期間中にフリーソフトウェアをいくつかリリースする予定です。
<!--more-->


### リリース準備ができているフリーソフトウェア

以下のフリーソフトウェアはリリース準備ができているので、会場のネットワーク環境をうまく使えれば期間中にリリースします。

  * [milter manager](/software/milter-manager.html)
  * [Ruby/groonga](http://groonga.rubyforge.org/)
  * [ActiveGroonga](http://groonga.rubyforge.org/)
  * [ActiveLdap](http://ruby-activeldap.rubyforge.org/)
  * [ActiveSambaLdap](http://asl.rubyforge.org/)
  * [test-unit](http://test-unit.rubyforge.org/)

午前11時頃か午後3時頃にスポンサーブース内でリリース作業をする予定です。
日本Ruby会議2009は3日間あるので、リリース作業はそれぞれの日に分散させる予定です。

### リリースできる可能性があるフリーソフトウェア

もしかしたら、以下のフリーソフトウェアもリリースできるかもしれません。

  * [Rabbit](http://www.cozmixng.org/~rwiki/?cmd=view;name=Rabbit)

1つ実装したい機能が残っているので、それが実装できればリリースします。が、時間的に厳しそうです。

### リリースしたかったフリーソフトウェア

以下のフリーソフトウェアもリリースしたかったのですが、準備が十分ではないので、おそらくリリースできないと思います。残念です。

  * [Ruby-GNOME2](http://ruby-gnome2.sourceforge.jp/)

Ruby/Libglade2にGC関連のSEGVバグがあり、それを修正する時間がとれなかったので、日本Ruby会議2009のタイミングではリリースできそうにありません。

また、ソフトウェアではありませんが、GUI関連で[Ruby GUI調査2008](http://www.pressure.to/ruby_gui_survey/index-ja.html)のレポートを日本語に翻訳したものもリリースしたかった[^0]のですが、間に合いませんでした。Alexさんからは編集可能な形で原文をもらっているので、もし、協力してくれる方がいればkou@clear-code.comまでご連絡ください。

### まとめ

日本Ruby会議2009期間中にリリースを予定しているフリーソフトウェアを紹介しました。

[RSS Parser](http://www.cozmixng.org/~rwiki/?cmd=view;name=RSS%20Parser)や[rcairo](http://jp.rubyist.net/magazine/?0019-cairo)など、ここに挙げたフリーソフトウェア以外でも構いませんので、なにかコメントなどがあればスポンサーブースで声をかけてください。

[^0]: 永井さんがLightning Talksで[Ruby/Tkは本当にダメな子なのか？](http://rubykaigi.org/2009/ja/talks/17H05)というお話をするようですし。
