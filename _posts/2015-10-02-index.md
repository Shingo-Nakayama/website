---
tags:
- fluentd
title: FluentdのインメモリDump configの機能を使いメモリ上の設定をダンプするには
---
### はじめに

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加しています。
<!--more-->


また、Fluentdにはプラグインというしくみがあり、たくさんのプラグインが開発されています。
FluentdはGitHub上で開発されています。また、Fluentd自体はそこまで大きくないものの、柔軟性に重点を置いたコードになっているため、内部構造は複雑です。

### メモリ上の設定ダンプ機能とは

Fluentdはログなどのデータを集約したり、再配分を行うソフトウェアです。ソフトウェアの性質上、長時間の動作が想定されます。そのため、運用されてある程度経ってくると設定ファイルとメモリ上にある設定が乖離することがあります。また、設定ファイルを書き換えて試している時でもFluentdが使用しているメモリ上の設定と実際の設定ファイルに乖離が生じます。

そのため、今の設定が正しく読み込まれているか、また現在の設定はどうなっているか確認したいという要望がFluentdの[Issue](https://github.com/fluent/fluentd/issues/633) に登録されていました。

この機能を実際に作成してv0.12.16へ取り込まれました。メモリ上の設定がダンプ出来るようになり、更に便利になったFluentdですが、注意点もあるので secret parameterと絡めての情報を紹介します。

#### 使い方

次のようにFluentdのRPC機能を有効化することでFluentdが使用しているメモリ上の設定のダンプAPIが有効になります。

```text
<system>
  rpc_endpoint 127.0.0.1:9000
</system>
```


この設定をFluentdの設定ファイルへ書いた後、`http://<rpc_endpoint>/api/config.dump` をcurlなどから以下のようにアクセスします。実行した結果、 `{"ok":true}` が返ってくれば成功です。

```text
$ curl http://127.0.0.1:9000/api/config.dump
{"ok":true}
```


すると、Fluentdの実行されているサーバー上でFluentdが使用している設定がログに出力されます。

```text
2015-10-02 11:26:29 +0900 [info]: <ROOT>
  <system>
    rpc_endpoint 127.0.0.1:9000
  </system>
</ROOT>
```


のようなログが出力されます。また、HTTP経由でメモリ上の設定をダンプする機能もあります。

しかし、後述する `secret parameter` の対応がFluentdのプラグインに行き渡っていない現状があるため、v0.12ではこの機能はデフォルトでは無効化されています。将来のバージョンではこの制限は取り除かれ、デフォルトで有効になる予定です。

HTTP経由でFluentdのインメモリの設定を取得する機能を有効化するには次のように `enable_get_dump true` を追加します。

```text
<system>
  rpc_endpoint 127.0.0.1:9000
  enable_get_dump true
</system>
```


curlなどを用いて `http://<rpc_endpoint>/api/config.getDump` へアクセスすると、`conf` キー以下にFluentdが現在使用している設定が入ってくるようになっています。

```text
$ curl http://127.0.0.1:9000/api/config.getDump
{"conf":"<ROOT>\n  <system>\n    rpc_endpoint 127.0.0.1:9000\n    enable_get_dump true\n  </system>\n</ROOT>\n","ok":true}
```


### メモリ上の設定ダンプ機能とsecret parameter

Fluentd v0.12.13からは `secret parameter` という機能が入りました。この機能は、`config_param` でプラグインの設定している箇所に `secret: true` のHash値を追加出来るようにしている機能です。

この `secret: true` （古いHashの記法では`:secret => true`） はv0.12.13より前のバージョンではエラーにならず、単に無視される挙動をします。

前述のメモリ上の設定のダンプ機能を有効化している際にはプラグインが `secret parameter` に対応しているかどうか確認してください。

例えば、データベースのパスワードや、AWSのAPIトークンやシークレットのような外部に制限なく公開されるのは望ましくないプラグインの設定値を設定している `config_param` を見つけたら `secret: true` が追加されているかを確認しておくと良いでしょう。

`secret parameter` 対応の例として、fluent origanization以下にあるfluent-plugin-mongoというプラグインへの対応を挙げます。このプラグインへの [pull request#54](https://github.com/fluent/fluent-plugin-mongo/pull/54) では、例えば `config_param :password, :string, :default => nil` へ `:secret => true` を追加する対応を行いました。
この対応により、メモリ上の設定をダンプしても重要なpasswordなどの情報は `xxxxxx` でマスクされ、見えなくなります。

### まとめ

Fluentd v0.12.16にはメモリ上の設定をダンプできるRPCのハンドラが追加されました。単純にFluentdの動いているサーバーのログへこの機能を使ってメモリ上の設定をダンプするのであれば、RPCを有効化し、新たなRPCハンドラにアクセスすればすぐに使い始めることができます。HTTP経由でのメモリ上の設定のダンプには現在のバージョンでは追加設定が必要な事も解説しました。

プラグインへ `secret parameter` が適切に設定されていれば、重要な情報を見せてしまうことなく、現在のFluentdの使用しているメモリ上の設定の情報を取得出来るようになります。

このメモリ上の設定のダンプ機能が取り込まれたRPCを使い始める際にはプラグインが `secret parameter` 機能に対応しているかどうかを確認し、パッチを送ることを試みてみるのはいかがでしょうか。
