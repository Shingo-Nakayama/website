---
tags:
- groonga
- presentation
title: '第28回 中国地方DB勉強会 in 岡山：Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索
  #ChugokuDB'
---
2020年1月25日(土)に[第28回 中国地方DB勉強会 in 岡山](https://dbstudychugoku.connpass.com/event/158127/)が開催されました。
私は、昨年の2019年11月に行われた [PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019) の内容(「Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索」)を再演させていただきました。
<!--more-->


これは、既存の技術を組み合わせて、なるべく楽に高速、高機能な全文検索ができる仕組みを紹介したものです。

当日使用した資料は、以下に公開しています。
[PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019) の資料とほぼ同じですが、 [PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019) で頂いた質問を元に一部加筆しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/dbstudychugoku-28th/viewer.html"
          width="640" height="520"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/dbstudychugoku-28th/" title="Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索">Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索</a>
  </div>
</div>


関連リンク：

  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/komainu8/dbstudychugoku-28th)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-dbstudychugoku-28th)

### 内容

内容については、[PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019) の再演となりますので、[PostgreSQL Conference Japan 2019：Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索 #pgcon19j]({% post_url 2019-11-18-index %})の内容とほぼ同じです。

ただ、[PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019) にて、この構成は同期レプリケーションと非同期レプリケーションのどちらを使っているのか？という質問をいただいていたので、この質問についての回答を追記しています。(スライドの85 - 90ページが追記した内容です。)

以下に追記した内容について記載します。

同期、非同期どちらのレプリケーションでもAmazon RDS + Amazon EC2 + ロジカルレプリケーション + PGroongaを使った低コスト高速全文検索の構成を使うことができます。

ただ、同期、非同期のレプリケーションを使った構成にはそれぞれ以下の特徴があるので、これらの特徴を踏まえた上でユースケースに合わせて選択することをおすすめします。

  * 同期レプリケーション

    * 同期レプリケーションでは、Subscriberの更新を待ってから応答を返すため、非同期レプリケーションに比べて更新性能は落ちてしまう。

    * Subscriberの更新を待つので、PublisherとSubscriberのデータは同一であることが保証される。

  * 非同期レプリケーション

    * 非同期レプリケーションでは、Subscriberの更新を待たずに応答を返すため、同期レプリケーションに比べて更新性能は高い。

    * Subscriberの更新を待たないので、PublisherとSubscriberのデータが同一でないことがある。(タイミングによっては、更新前のデータが見えてしまう。)

同期レプリケーションでは、PublisherとSubscriberのデータが同一であることが保証されるので、更新したデータを即時検索できることがとても重要な場合は、同期レプリケーションを選択することをおすすめします。

そうではない場合は、非同期レプリケーションを選択して更新性能を落とさず検索できるようにすることをおすすめします。

### まとめ

今回の発表にあたって、追記した内容を中心に紹介いたしました。
中国地方DB勉強会の皆様、中国地方のコミュニティでの発表という貴重な機会をいただきありがとうございました。

最後に、PGroongaを使った全文検索について、興味、疑問などございましたら、是非、[お問い合わせ](/contact/)ください。
