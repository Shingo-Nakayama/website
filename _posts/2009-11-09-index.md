---
tags:
- mozilla
- presentation
title: Firefox Developers Conference 2009にて発表を行いました
---
こんにちは。下田（Piro）です。
<!--more-->


昨日2009年11月8日に開催された[Firefox Developers Conference 2009](http://mozilla.jp/events/2009/fxdevcon/)にて、トークセッション「Aza Raskin に一問一答！」にパネリストとして参加しました。また、懇親会でのライトニングトーク第2部にて発表を行いました。参加された皆様、お疲れ様でした。また、セッションにお越しいただいた皆様、誠にありがとうございます。

発表資料および映像は以下よりご覧いただけます。

  * [トークセッション「Aza Raskin に一問一答！」録画映像](http://qik.com/video/3472269)（※同時通訳の音声は録音されていませんのでご注意下さい）
  * [ライトニングトーク「Webアプリとハードウェアを繋げたい！」発表資料](/archives/fxdevcon2009/lt.xul)（※閲覧にはFirefoxまたはGeckoエンジン使用のWebブラウザが必要です）
  * [ライトニングトーク「Webアプリとハードウェアを繋げたい！」録画映像](http://www.youtube.com/watch?v=84-lNFYC7ME)

<div class="youtube-4x3">
  <iframe width="425"
          height="350"
          src="https://www.youtube.com/embed/84-lNFYC7ME"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>
</div>

また、ライトニングトーク中で紹介しているアドオン「システムモニター」の最新版であるVer.0.3は、以下からダウンロードできます。

  * [packages.clear-code.comからダウンロード](http://packages.clear-code.com/xul/extensions/system-monitor/system-monitor-0.3.xpi)
  * [addons.mozilla.orgからダウンロード](https://addons.mozilla.org/firefox/addon/15093)

以下、イベントの感想と、イベント中に語ることができなかった点について書いていきたいと思います。

### Aza Raskinさん、天野（amachang）さんとのトークセッション「Aza Raskin に一問一答！」について

このセッションでは、次期Firefoxでの標準搭載も視野に入れて開発が進められている[Jetpack](https://jetpack.mozillalabs.com/)や、新しいUIの可能性を模索する[Ubiquity](http://ubiquity.mozilla.com/)の開発者として知られるAza Raskinさんに対して、現在Firefoxアドオンを開発している・開発した経験がある人の立場からの質問を私が、主にWebアプリケーションの開発を行っておりこれからJetpack用のスクリプト（Jetpack feature）の開発に携わる可能性がある人の立場からの質問を天野さんが行う形で、Firefoxの今後とWebアプリケーションの将来像について様々な話を聞かせていただきました。

Firefoxはアドオン（拡張機能）によるカスタマイズ性の高さが最大の特長ですが、自由度の高さゆえに、アドオンの開発には様々な準備と知識が必要になるという欠点もあります。Jetpackは、この欠点をカバーしてFirefoxの機能を拡張するスクリプトをより容易に配布・利用できるようにする物として開発されている、新しいアドオンのメカニズムです。「拡張機能」がテーマである今回のFirefox Developers Conference 2009においては、避けて通れない話題ですね。

今回のトークセッションにあたって事前に[いくつかのJetpack featureを開発](http://piro.sakura.ne.jp/latest/blosxom?-tags=Jetpack)したのですが、その際に最も強く感じた事は、APIについての不満でした。現状ではAPIに関するドキュメントが圧倒的に不足しており、また、APIがドラスティックに変更されている最中であるため、安定したJetpack featureの開発および利用はまだまだ難しいのが実情です。セッション中でも質問を行いましたが、これについては、むしろ今が開発に口出ししてAPIをより良い物にしていくチャンスと捉えてほしいという回答をいただきました。とはいえ、APIドキュメントの重要性は彼らの間でも認識されており、そのための体制を整えているところであるとの話も聞けました。

Azaさんは、[セカンドシステム症候群](http://suto3.mo-blog.jp/nashita/2006/03/second_system_s_ca7a.html)に陥ってはならないという事を再三述べていました。これまでのアドオンの仕組みに多くの不満があるからといって、その代替として新たに登場してきたJetpackに過剰な要求をして完璧な物を最初から求めると、議論が紛糾するばかりで実装が停滞し、Jetpackという仕組み自体がいつまで経っても完成しなくなってしまいます。ほどほどの所でまずは皆に使ってもらい、その上で実際の利用状況を見ながら少しずつ不足箇所を補っていく、という姿勢が重要ですね。

Jetpack featureで利用可能なライブラリとしては、現在はjQueryが標準添付されていますが、ゆくゆくはPerlにおけるCPANのように、様々なライブラリを簡単に利用できるようにする計画もあるそうです。また、現状のJetpackも将来のバージョンも、jQueryを使うことは必須というわけではなく、ライブラリの機能を使わずにJetpack featureを開発しても問題は無いとのことです。なお、私も天野さんも既存のライブラリをほとんど利用していない理由について、Azaさんより逆に質問をされるという場面もありました。（天野さんの場合は主に既存のライブラリのオーバーヘッドが気になるためで、私の場合はFirefox用アドオンの開発のみを行う都合上、ライブラリの最大のメリットである「ブラウザ間の差異の吸収」の恩恵を受けにくいというのが理由です。）

天野さんがWebアプリの開発者の観点から投げかけられた質問として、WebのUIを良くするにはどのようにすればよいか？という物もありました。これについてAzaさんは、作り手側が絶対に手出し・口出ししないようにして、目の前でエンドユーザに実際に使ってもらいその様子を観察すれば、WebのUIはもっと良くなるだろうとおっしゃっていました。ユーザビリティ・テストはUI開発の基本と言えますが、DTPなど視覚的デザインの文脈で捉えられることがまだまだ多いWebの世界においても、きちんと「インターフェース」としての作り方を意識する必要があるということですね。

国ごとの文化の違いがUIに与える影響について、Azaさんは欧米と中国のWebブラウズの仕方の違いを例に挙げてお答え下さいました。曰く、欧米や日本では1つの画面にかじりついて操作するのが一般的であるのに対し、中国では画面から少し離れて、画面内に非常に多くの機能・要素を詰め込んでそれらを平行して利用する場合が多いそうです（そのため、Googleのかつてのシンプルなトップページは不評だったそうです）。そういった文化ごとの利用形態の違いに合わせるために、末端にいる人達にUIのデザインを行う権限を持たせる必要があるのではないか、という意見も示されていました。

他のブラウザの隆盛、特にWebKitが多数のプロジェクトで採用されている事についての質問もありました。これについては、かつては止まったWebブラウザの進化をFirefoxが刺激して活性化させたように、今はWebKitがWebを活性化させているとして、その結果Webがより便利になるのであれば最終的な勝者は我々エンドユーザだ、との考えをお答えいただきました。

というように、Jetpackの話題のみにとどまらず、WebのUI一般の話にまで言及したトークセッションとなりましたが、皆様お楽しみいただけましたら幸いです。

### ライトニングトーク「Webアプリとハードウェアを繋げたい！」について

ライトニングトークへの参加希望者が多かったため、過去に発表経験のある数人については懇親会の場で発表を行うこととなりました。今回は[システムモニター]({% post_url 2009-10-20-index %})で行った、アドオンによるWebアプリケーション向けAPIの提供方法について発表しました。

「scriptable hardware」というキーワードで、基調講演でChris BlizzardさんがFirefoxの加速度センサー対応やカメラ用APIの実験について語っていらっしゃいました。クリアコードでも独自に同様の研究を行っており、システムモニター以外にも、指紋認証アドオン「Fingerprint-Auth」の実装を進めております。また、その開発の過程で浮上してきたアイデアとして、USB機器全般をJavaScriptから制御できるようにしてみてはどうかという話もあり、それについても「進行中のプロジェクト」としてお話しさせていただきました。Fingerprint-Authについては実際にお試しいただける形の物を近日中に公開する予定ですので、どうぞお楽しみに。

セッション「js-ctypes ～ネイティブコードを呼び出す新しいカタチ」では、JavaScriptからプラットフォームネイティブなバイナリの機能を呼び出すより平易な方法を提供するFirefox 3.6からの新機能[js-ctypes](https://developer.mozilla.org/ja/JavaScript_code_modules/ctypes.jsm)を紹介されていましたが、システムモニターやFingerprint-AuthのようにJavaScriptのグローバルな名前空間にAPIを提供しようと思うと、現状ではXPCOMコンポーネントとして実装を行う必要があります。また、XPCOMコンポーネントはC++での開発となるため、各プラットフォーム向けのビルドやnsISecurityCheckedComponentインターフェースの実装などの作業が必要となります。残念ですね。

なお、発表ではすべての実装がJavaScriptによって行われている[XUL/Migemo](http://piro.sakura.ne.jp/xul/_xulmigemo.html)でのAPIの提供方法を例として紹介しました。JavaScript製XPCOMコンポーネントの場合は、比較的簡単に機能をWebアプリケーション向けのAPIとして公開することができます。作成手順については[Firefox 3 Hacks](https://amazon.co.jp/dp/487311375X)等の解説と[発表資料中の説明](/archives/fxdevcon2009/lt.xul)を併せてご覧下さい。

### セッション「はてなブックマーク Firefox 拡張の裏側」について

[株式会社はてなさん](http://www.hatena.ne.jp/)の[はてなブックマークFirefox拡張](http://b.hatena.ne.jp/guide/firefox_addon)についての発表では、開発に使用したツールとして弊社開発のテスティングフレームワーク[UxU](/software/uxu/)をご紹介いただけました。主にデータベース関連の機能等のユニットテストに利用されているとのことで、非同期処理が絡むテストの開発に役立ったとのお言葉をいただけたのは嬉しかったです。また、私が個人的に開発している各種のアドオンについても、ソースコードをアドオン開発の参考にしていただけたそうで、ありがたい限りです

なお、UxUでは最近のバージョンアップで[より使いやすいユーザ操作エミュレーション用のAPI](/software/uxu/helpers.html#actions)が追加されました。基本部分のユニットテスト以外にも、GUIが関係する機能テストやインテグレーションテストにもUxUを活用していただけたらと思っております。

### イベント全体について

UbiquityにもJetpackにも共通している1つの狙いとして、「より多くの人が開発を行えるようにする」という目標があるそうです。より簡単に開発できるようになり、より多くの人が開発できるようになれば、より多くのイノベーションが生まれる、というのは前回のFirefox Developers Conferenceの際にもAzaさんが語られていた話ですが、[AutoPagerize](http://autopagerize.net/)、ひいてはその原点である[GoogleAutoPager](http://la.ma.la/blog/diary_200506231749.htm)が、どちらもGreasemonkeyスクリプトという世界から登場したことは、まさにその象徴的な出来事だと思います。

これはFirefoxのアドオン開発のみに限られた話ではなく、Chris Blizzardさんが語ったキーワード「scriptable hardware」もそれに連なる話であると言えるでしょう。折しも、先日Googleが[Closure Tools](http://code.google.com/closure/)という、JavaScriptでのアプリケーション開発のためのツール集を[公開しました](http://japan.cnet.com/news/ent/story/0,2000056022,20403058,00.htm)。APIやツールなど様々な側面から、Webクライアント上で動作する高機能なアプリケーションを開発しやすくするための土壌が整いつつあります。Webアプリケーションがエンドユーザからただの「アプリケーション」として扱われるようになる、Web OSという未来が本当にすぐそこまで来ているのだということを、改めて意識させられたイベントでした。

### 感想・イベントレポートリンク集

  * [Twitter ハッシュタグ「#fxdevcon2009」検索結果一覧](http://twitter.com/search?q=%23fxdevcon2009)
  * [Mozilla lab のラーメンギークことAzaに尋ねてみた「Web OSの可能性とFirefox、Apple、Googleの立ち位置」（Firefox Developers Conference 2009にて） - つぶやくには長すぎた](http://d.hatena.ne.jp/ID2nd/20091109/1257783453)
  * [Mozilla Japan、開発者と利用者をつなぐコミュニティサイト「modest」公開:ニュース - CNET Japan](http://japan.cnet.com/news/media/story/0,2000056023,20403213,00.htm)
  * [【レポート】開発者が憧れる開発者 - Firefoxデベロッパのトークセッション - エンタープライズ - マイコミジャーナル](http://journal.mycom.co.jp/articles/2009/11/16/firefoxdev/index.html)
  * [Firefox 誕生5周年 &amp; Firefox Developers Conference 2009 - えむもじら](http://level.s69.xrea.com/mozilla/index.cgi?id=fxdevcon2009)
  * [Firefox Developers Conference 2009 に行ってきました « きんくまデザイン](http://www.kuma-de.com/blog/1-diary/2009-11-09/1416)
  * [Firefox Developers Conference 2009に参加してきました。 - E-riverstyle Vanguard - CSSやXHTML,Javascriptやweb製作に関する事を紹介](http://blog.e-riverstyle.com/2009/11/firefox-developers-conference.html)
  * [Firefox Developers Conference 2009にいってみて - degのはてなダイアリー](http://d.hatena.ne.jp/deg84/20091109/1257780656)
  * [Firefox Developers Conference 2009 : せつないぶろぐ](http://blog.setunai.net/20091109/firefox_developers_conference_2009/)
  * [Firefox Developers Conference 2009 - bathgrammer](http://d.hatena.ne.jp/moira/20091109/1257782937)
  * [Mozilla lab のラーメンギークことAzaに尋ねてみた「Web OSの可能性とFirefox、Apple、Googleの立ち位置」（Firefox Developers Conference 2009にて） - つぶやくには長すぎた](http://d.hatena.ne.jp/ID2nd/20091109/1257783453)
  * [FDC 2009 行ってきた - 野菜](http://d.hatena.ne.jp/ssig33/20091109/1257740760)
  * [Firefox Developers Conference 2009 アウトラインメモ - Web scratch](http://efcl.info/2009/1108/res1424/)
  * [Firefox Developers Conference 2009 行ってきたよ！＆Firefox 5 周年おめでとう！ - 徒然なる記録 - Chaotic Shore](http://www.chaoticshore.org/blog/2009/11/09-123739.html)
  * [Firefox Developers Conference 2009 に参加して来ました - A Better Project＠はてなダイアリー](http://d.hatena.ne.jp/potappo/20091109/1257702497)
  * [Firefox Developers Conference 2009 - フリーフォーム フリークアウト](http://d.hatena.ne.jp/cou929_la/20091108/1257691449)
  * [Firefox Developers Conference 2009 の感想。 - trash-area.com](http://trash-area.com/archives/662)
  * [Firefox Developers Conferenceに参戦 - ぽりぴぃすらいと](http://d.hatena.ne.jp/muddydixon/20091108/1257641779)
  * [Firefox Developers Conference 2009: Days on the Moon](http://nanto.asablo.jp/blog/2009/11/12/4688127)
  * [Firefox Developers Conference 2009 に行ってきました - ヨモツネット](http://www.yomotsu.net/wp/?p=548)
  * [Firefox Developers Conference 2009 レポート](http://www.tm256.biz/archives/2009/11/firefox_develop.html)
  * [Firefox Developers Conference 2009 - Ussy Diary(2009-11-08)](http://www.pshared.net/diary/20091108.html)

### 発表資料リンク集

  * [Firefox Developers Conference 2009 発表資料 - 川o・-・）＜2nd life](http://d.hatena.ne.jp/secondlife/20091109/1257741302)：セッション「はてなブックマーク Firefox 拡張の裏側」資料
  * [Electronic Genome - Firefox Developers Conference 2009 LT 発表資料](http://itmst.blog71.fc2.com/blog-entry-187.html)：ライトニングトーク「Dropfox @ Lightning Talk」資料
  * [Firefox Developers Conference 2009 ライトニングトーク発表資料 - SWDYH](http://d.hatena.ne.jp/swdyh/20091108/1257706815)：ライトニングトーク「AutoPagerize on Firefox and Google Chrome」資料
  * [SQLiteAnalyzer(仮)](http://teramako.github.com/doc/FxDevCon2009/fxdevcon2009.html)：ライトニングトーク第2部「SQLiteAnalyzer(仮)」資料
  * [Webアプリとハードウェアを繋げたい！](/archives/fxdevcon2009/lt.xul)：ライトニングトーク第2部「Webアプリとハードウェアを繋げたい！」資料
