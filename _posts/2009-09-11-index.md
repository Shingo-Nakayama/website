---
tags:
- ruby
title: とちぎRuby会議02参加登録受付中
---
10月24日に開催される[とちぎRuby会議02](http://regional.rubykaigi.org/tochigi02)の参加登録が行われています。[懇親会参加の状況](http://atnd.org/events/1488)から推測するとまだ登録できそうです。
<!--more-->


今回は、[超優良企業](http://d.hatena.ne.jp/m_seki/20090819#1250680491)の1つとして声をかけてもらい、出場者として参加できることになりました。[ワイクル](http://www.waicrew.com/)の[角さん](http://capsctrl.que.jp/kdmsnr/)、[ヴァインカーブ](http://vinecaves.com/)の[やまだあきらさん](http://arika.org/)、タワーズ・クエストの[和田さん](http://d.hatena.ne.jp/t-wada/)も出場者として参加されます。とても楽しみですね。

[咳さん](http://d.hatena.ne.jp/m_seki/)の[dRuby本](http://www.druby.org/ilikeruby/dRubyBook2.html)をテキストとしたtoRuby勉強会も開催されるそうです。こちらもとても楽しみですね。

都合があう方は参加してみてはいかがでしょうか。
