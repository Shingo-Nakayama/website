---
title: Firefoxのポリシー設定のExtensionsとExtensionSettingsの使い分け
author: piro_or
tags:
- mozilla
---

結城です。

Firefoxのポリシー設定には、名前も役割も非常に似ている[`Extensions`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensions)と[`ExtensionSettings`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensionsettings)という2つの設定があります。
この度、これらの違いについてお客さまからお問い合わせを頂く機会がありました。

この記事では、この両者をどう使い分けるのか、どちらを使えばよいのかを解説します。

<!--more-->

### 2つの設定の違い

[`Extensions`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensions)と[`ExtensionSettings`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensionsettings)の2つの違いは、端的に言えば、前者はFirefox独自のポリシー設定で、後者は[Google Chrome用のポリシー設定](https://support.google.com/chrome/a/answer/9867568?hl=JA)とある程度互換性があるポリシー設定である、ということになります。

#### `Extensions`

元々、Firefoxにポリシー設定機能が実装された際に用意されたのが`Extensions`でした。
こちらの設定は、以下の特徴があります。

* グループポリシーの完全なポリシーテンプレートが用意されており、「管理用テンプレート」→「Mozilla」→「Firefox」→「Extensions」配下の「Extensions to Install」「Extensions to Uninstall」「Prevent extensions from being disabled or removed」からGUIで設定を行える。
* `Install`は、パッケージのURL（File URL含む）を列挙するだけで使用できる。
* `Uninstall`および`Lock`は、アドオンの識別子を列挙するだけで使用できる。
* *冪等性がない*。`Install`と`Uninstall`は、各端末のFirefoxにおいて最後にこれらのポリシー設定を反映したときの設定内容が保存されるため、以降は、*ポリシー設定の内容に変化がないかぎりは、各端末のFirefoxで同じポリシーが再適用されることはない*。
  * 例えば、全端末にあるアドオンがインストールされている状況で、`Uninstall`にそのアドオンをアンインストールするための指定を登録したとすると、その後のFirefoxの起動時に当該アドオンはアンインストールされるが、その後ユーザーが任意でもう一度アドオンをインストールし直した場合、「ポリシー設定の内容には変更が無い」ため、当該アドオンが再度アンインストールされることはない。

#### `ExtensionSettings`

その一方で、`Extensions`の実装から約半年ほど後に、よりきめ細やかな管理を行えるようにするためGoogle Chrome用のポリシー設定の形式を参考にして（完全互換ではなく、いくつかはFirefox独自の機能を加えて）実装されたのが`ExtensionSettings`です。
こちらは、以下の特徴があります。

* グループポリシーのポリシーテンプレートが用意されているものの、内容は非常にざっくりしている。「管理用テンプレート」→「Mozilla」→「Firefox」→「Extensions」配下の「Extension Management」から設定を行えるが、入力内容は「適切に記述したJSONをそのまま貼り付ける」という形式のため、管理の難易度が高い。
* 各アドオンごとに、以下の設定を行える。
  * `installation_mode`によるインストールの可否の細かな制御。値は以下のいずれか[^installation-mode-removed]。
    * `allowed`（ユーザー権限でのインストールを許可）
    * `blocked`（ユーザー権限でのインストールを禁止し、インストール済みの場合は必ず削除する）
    * `force_installed`（ユーザー権限でのアンインストールを禁止し、未インストールであれば必ずインストールし直す）
    * `normal_installed`（未インストールであればインストールするが、ユーザー権限で無効化できる）
  * `install_url`によるパッケージのURL（File URL含む）の指定。
  * `updates_disabled`による、アドオン自体の自動更新の可否の制御[^updates-disabled]。値は以下のいずれか。
    * `true`（自動更新を行わない）
    * `false`（自動更新を行う）
  * `default_area`による、拡張機能のツールバーボタンの初期配置[^default-area]。値は以下のいずれか。
    * `navbar`（ナビゲーションツールバー）
    * `menupanel`（オーバーフローパネル内）
* `*`を識別子の代わりに指定して、全アドオンに適用される既定の設定を制御できる。具体的には、各アドオン毎に行える設定に加え、以下を設定できる。
  * `install_sources`でのURLパターン指定による、Webからのインストールの可否の制御。
  * `allowed_types`による、インストールを許可するアドオンの種類の限定。値は以下のうち1つ以上を含む配列[^allouued-types]。
    * `extensions`（拡張機能）
    * `theme`（テーマ）
    * `dictionary`（スペルチェック辞書）
    * `locale`（言語パック）
  * `restricted_domains`でのドメイン指定による、コンテンツスクリプトの実行可否の制御。初期設定での「Mozilla公式サイトではスクリプトの注入を禁止する」という動作と同様の制限を行える[^restricted-domains]。
* *冪等性がある*。全ての設定は、*前回の適用時の成否にかかわらず、Firefox起動時（ポリシーの反映時）に自動適用される*。
  * 例えば、全端末にあるアドオンがインストールされている状況で、`"addon-id":{"installation_mode":"blocked"}｀を登録したとすると、その後のFirefoxの起動時には当該アドオンはアンインストールされる、その後ユーザーが任意でもう一度アドオンをインストールし直せない。何らかの方法で制限を突破してインストールできたとしても、次回以降の起動時のポリシー設定反映時に当該項目がまた削除される。

[^installation-mode-removed]: Google Chrome用のポリシー設定には`removed`という選択肢がありますが、Firefoxでは`blocked`がその役割を兼ねます。
[^updates-disabled]: Firefox独自の設定で、Google Chrome用のポリシー設定にはない機能です。
[^default-area]: Firefox独自の設定で、Google Chrome用のポリシー設定にはない機能です。
[^allouued-types]: Google Chrome用のポリシー設定には`hosted_app`、`legacy_packaged_app`、`platform_app`、`user_script`という選択肢がありますが、Firefoxはこれらには対応していません。その代わりに、Firefox独自の選択肢として`dictionary`と`locale`があります。
[^restricted-domains]: Google Chrome用のポリシー設定の`runtime_allowed_hosts`の代わりとなる、Firefox独自の機能です。

### どう使い分けるべきか

#### 新しい方・高機能な方がよいという観点

一時期、「`Extensions`は非推奨の古い設定で、`ExtensionSettings`を使うように」と誘導されていたことがありました。
本記事の執筆時点では「非推奨」の表記は取り下げられていますが、`ExtensionSettings`の方が高機能で、`Extensions`で可能なことは全て`ExtensionSettings`でもカバーされていて、`ExtensionSettings`の方を使うことが推奨されています。
機能の面では、`Extensions`を今敢えて使う必然性は全く無いと言っていいでしょう。

#### 冪等性がある方がよいという観点

筆者としては、両者は冪等性の有無が最大の違いだと考えています。

* `Extensions`は「アクション」を定義する性格の物であるために、一度反映された後はポリシーの設定内容が更新されるまで再適用されない（冪等性がない）。
* `ExtensionSettings`は「状態」を定義する性格の物であるために、毎回の起動時に必ずポリシーでの設定が反映される（冪等性がある）。

`Extensions`で狙ったとおりの効果を得るためには、クライアントのユーザープロファイルの内容・前回の実行時の情報を正確に把握していないといけません。
`ExtensionSettings`ではそのような注意が必要無いため、その点でも、管理は容易になっていると言えます。

#### 設定の反映が容易な方がよいという観点

ただし、`ExtensionSettings`は「グループポリシーでは設定しにくい」という弱点があります。
`ExtensionSettings`はグループポリシーで設定する場合にもJSON形式の文字列を組み立てる必要があるため、「ポリシーテンプレートによってGUIで設定を行えるため、技術的に詳しい知見がなくても容易に管理できる」というグループポリシーの利点を享受できないからです。
Firefoxを普段管理する人が、エラーのないJSON文字列を正確に組み立てるのが難しい場合には、`Extensions`の方を使うのが安全かもしれません。

`policies.json`でポリシーを適用している場合であれば、両者の間で設定の手間にはそれほど差異はありません。
この場合、迷わず`ExtensionSettings`を使うのが得策でしょう。



### まとめ

Firefoxのポリシー設定の`Extensions`と`ExtensionSettings`について、それぞれの特徴と、どちらを使うべきか（基本的には`ExtensionSettings`がよいが、グループポリシーでの管理を行っていて、管理担当者が技術的にあまり明るくない場面に限っては、`Extensions`を使う方が安全な場合がある、という使い分けの判断基準）を説明しました。

当社では、Firefoxの法人運用におけるトラブルの原因究明や回避方法の調査、設定のはたらきが不明な部分の詳細な調査などを有償にて承っております。
本記事の内容も、お客さま環境でのFirefox ESR115以降に際しての検証の過程で発覚したものです。
Firefoxの運用でお悩みの企業のご担当者さまで、このようなレベルでの調査・対応を必要とされている方は、[お問い合わせフォームよりご相談ください]({% link contact/index.md %})。
