---
tags:
- fluentd
title: 最小構成のFluent Loggerを作成するには
---
### はじめに

クリアコードでは[Fluentd](http://www.fluentd.org)というソフトウェアの開発に参加しています。Fluentdはログ収集や収集したデータの分配・集約などを行うソフトウェアです。
<!--more-->


また、Fluent Loggerと呼ばれるFluentdへデータを転送出来る実装が各言語で公開されています。

Fluentdのv0.10とv0.12のイベントの転送の仕組みはForward protocolとして仕様化されています。登場したばかりの新しいプログラミング言語でFluent Loggerがまだない場合、この仕様に基づいて自分で実装する必要があります。

### Forward Protocol v0

この記事ではFluentd v0.10とv0.12で用いられている[Forward Protocol v0](https://github.com/fluent/fluentd/wiki/Forward-Protocol-Specification-v0)を元に解説します。

#### Forwardプロトコル

Fluent Loggerでは次の3つのうちどれかを実装している必要があります:

  * Message Mode

  * Forward Mode

  * PackedForward Mode

このうち、Fluent loggerの実装によく使われているのがMessage modeと呼ばれる形式です。現在のところPackedForward対応を謳っているFluent logger実装は多くありません。[^0]

この記事ではMessage modeでの最小構成のFluent Loggerの作り方について解説をするため、Message modeに絞って解説します。

#### Message modeとは

Message modeは一度に一つのイベントのみを送ることができます。また、そのイベントの中身は次の通りです。

  * タグ

  * Unix時間

  * レコード

  * オプション

このうち、オプションは必須ではありません。そのため、タグ・Unix時間・レコードをひとまとめにしてForwardプロトコルを実装しているサーバーに送ることが出来ればよい、となります。

また、送る形式はJSONまたはmsgpackのどちらかを選択する事ができます。

#### Message modeでイベントを送るには

ここまででMessage modeでイベントを送るための形式が分かりました。実際にMessage modeでイベントをForward protocol v0を実装したサーバーに送る際には接続の種類や、接続の切断まで仕様から読み解く必要があります。
では、その仕様を見てみましょう。その仕様はFluentdのWikiの以下の部分に書かれています:

  * [Option | Forward Protocol Specification v0](https://github.com/fluent/fluentd/wiki/Forward-Protocol-Specification-v0#option)

  * [Response | Forward Protocol Specification v0](https://github.com/fluent/fluentd/wiki/Forward-Protocol-Specification-v0#response)

OptionとResponseの節でMessage modeでoptionを送らない場合はForwardプロトコルを実装しているサーバーは接続を切る動作をします。
なので、Message modeで送る際には以下のような流れでFluent LoggerはForwardプロトコルを実装するサーバーにイベントを送ります。

  1. Forwardプロトコルを実装しているサーバーにTCPで接続する

  1. タグ・Unix時間・レコードを含むイベントをForwardプロトコルを実装しているサーバーに送る

  1. Forwardプロトコルを実装しているサーバーとの接続を切る

という流れになります。また、TCP接続は毎回Forwardプロトコルを喋るサーバーから切られるものとしてロガーを実装してください。

### おわりに

Fluent Loggerに最低限必要な機能には何があるかを解説しました。以上はForwardプロトコルに載せてイベントを送る際に最低限必要な事柄です。
この方針で実装されたFluent Loggerはネットワーク上の問題やアプリケーションの異常によるログの消失を抑える工夫がなされていないため、これらを抑える工夫が必要になります。
その工夫については次の機会に解説します。

[^0]: 例えば、 <a href="https://github.com/komamitsu/fluency">Fluency | GitHub</a> はPacked Forwardを実装しているのを確認しています。
