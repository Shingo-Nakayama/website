---
tags: []
title: Emacs実践入門 - おすすめEmacs設定2012
---
2012年3月にEmacsの入門書が技術評論社から出版されました。
<!--more-->


[https://amazon.co.jp/dp/9784774150024](https://amazon.co.jp/dp/9784774150024)

インストール方法やファイルの開き方などから始まっていて初心者向けの始まり方になっています。それでは初心者向けなのかというとそうでもなく、中盤から後半は`require`しないと使えないElispを使った拡張方法の紹介になっています。

おそらく、初心者の人は1/3か1/2くらい進んだところで一度脱落するのではないでしょうか。逆に、ある程度知っている人は中盤から後半にかけて興味のある話題が増えていくことでしょう。脱落してしまった人は、しばらく前半の機能でEmacsを使って、慣れてきてから再挑戦するとよいでしょう。

後半の拡張方法の紹介部分では多くの方法を紹介するためか、1つ1つの方法については簡単に紹介する程度にとどまっています。よりつっこんだ使い方までは踏み込んでいません。そのため、すでに最近のEmacs界隈の状況を把握している人やバリバリカスタマイズして使っている人にとっては物足りない内容かもしれません。そうでない人は、こんな方法があるのかと気づくことも多いでしょう。

ということで、今ひとつEmacsを使いこなせていない感のある人は読んでみてはいかがでしょうか。Emacsがより手になじむことになるでしょう。

### おすすめEmacs設定2012

さて、Emacsが手になじむようになるには自分がEmacsに慣れるだけではなく、Emacsにも自分に歩み寄ってもらうことが近道です。そのためにEmacsの設定をカスタマイズします。

約1年前に[おすすめEmacs設定]({% post_url 2011-02-16-index %})を紹介しました。ここで紹介した設定は基本的なものだけに限定していましたが、より細かい設定や`require`しないと使えないElispの設定も増えています。

それでは、1年経ったおすすめEmacs設定を紹介します。

#### ディレクトリ構成

まず、ディレクトリ構成が変わりました。設定ファイルのディレクトリ構成はEmacs実践入門でも提案されていますが、ここではまた違った構成にしています。Emacs実践入門でも「筆者もこの設計がベストだとは思っておらず、より良い配置を模索中です。ぜひもっと優れた設計を考えてみてください。」[^0]と書かれているので、自分になじむ構成を見つけてください。

{% raw %}
```
.emacs.d
|-- init.el                ;; 基本的な設定を記述
|-- local.el               ;; （カスタマイズ用）
|-- config                 ;; 特定のモードや非標準のElispの設定をこの下に置く
|   |-- builtins.el        ;; 標準Elispの設定
|   |-- builtins           ;; 標準Elispのうち、設定が多くなるものはこの下に置く
|   |   |-- local.el       ;; （カスタマイズ用）
|   |   `-- cc-mode.el     ;; （例）標準Elispであるcc-modeの設定
|   |-- packages.el        ;; 非標準Elispの設定
|   |-- packages           ;; 非標準Elispのうち、設定が多くなるものはこの下に置く
|   |   |-- local.el       ;; （カスタマイズ用）
|   |   `-- sdic.el        ;; （例）非標準Elispであるsdicの設定
|   `-- el-get             ;; el-getの設定はこの下に置く
|       |-- recipies       ;; el-getのレシピはこの下に置く
|       `-- local-recipies ;; （カスタマイズ用）
`-- el-get                 ;; el-get管理のパッケージをこの下に置く
```
{% endraw %}

1年前までは`package.el`という名前の独自のパッケージ管理システムを使っていたのですが、同じ名前のパッケージ管理システムがEmacs 24に標準搭載されることになったため、[el-get](https://github.com/dimitri/el-get)に乗り換えました。el-getにした理由は元々使っていたパッケージ管理システムと同じことができたからです。

#### init.el: 基本的な設定

それでは、まず、基本的な設定を説明します。

##### ロードパス

以前は`~/.emacs.d/packages`もパスに入っていましたが、el-get管理になったので除きました。

{% raw %}
```
;;; ロードパスの追加
(setq load-path (append
                 '("~/.emacs.d")
                 load-path))
```
{% endraw %}

##### 日本語環境

{% raw %}
```
;;; Localeに合わせた環境の設定
(set-locale-environment nil)
```
{% endraw %}

##### キーバインド

C-hの設定を`define-key`ではなく`keyboard-translate`を使うようにしました。`c-electric-backspace`を明示的に`define-key`しなくてもよいことに気づいたからです。しかし、キー入力中に`C-h`を押してもキーバインド一覧が出てこないのは不便なので`define-key`に戻すかもしれません。

また、ウィンドウ移動用のキーバインドも追加しました。

{% raw %}
```
;; C-hでバックスペース
;; 2012-03-18
(keyboard-translate ?\C-h ?\C-?)
;; 基本
(define-key global-map (kbd "M-?") 'help-for-help)        ; ヘルプ
(define-key global-map (kbd "C-z") 'undo)                 ; undo
(define-key global-map (kbd "C-c i") 'indent-region)      ; インデント
(define-key global-map (kbd "C-c C-i") 'hippie-expand)    ; 補完
(define-key global-map (kbd "C-c ;") 'comment-dwim)       ; コメントアウト
(define-key global-map (kbd "M-C-g") 'grep)               ; grep
(define-key global-map (kbd "C-[ M-C-g") 'goto-line)      ; 指定行へ移動
;; ウィンドウ移動
;; 2011-02-17
;; 次のウィンドウへ移動
(define-key global-map (kbd "C-M-n") 'next-multiframe-window)
;; 前のウィンドウへ移動
(define-key global-map (kbd "C-M-p") 'previous-multiframe-window)
```
{% endraw %}

便利なのが`M-C-g`のgrepです。grepにはまだ設定があります。

##### grep

{% raw %}
```
;; 再帰的にgrep
;; 2011-02-18
(require 'grep)
(setq grep-command-before-query "grep -nH -r -e ")
(defun grep-default-command ()
  (if current-prefix-arg
      (let ((grep-command-before-target
             (concat grep-command-before-query
                     (shell-quote-argument (grep-tag-default)))))
        (cons (if buffer-file-name
                  (concat grep-command-before-target
                          " *."
                          (file-name-extension buffer-file-name))
                (concat grep-command-before-target " ."))
              (+ (length grep-command-before-target) 1)))
    (car grep-command)))
(setq grep-command (cons (concat grep-command-before-query " .")
                         (+ (length grep-command-before-query) 1)))
```
{% endraw %}

-rオプションを追加して常に再帰的にgrepするようにします。grep-findなどを使い分けなくてもすみます。

##### 画像表示

{% raw %}
```
;;; 画像ファイルを表示
(auto-image-file-mode t)
```
{% endraw %}

バッファ内で画像ファイルを表示します。

##### バーを消す

{% raw %}
```
;;; メニューバーを消す
(menu-bar-mode -1)
;;; ツールバーを消す
(tool-bar-mode -1)
```
{% endraw %}

##### カーソル

{% raw %}
```
;;; カーソルの点滅を止める
(blink-cursor-mode 0)
```
{% endraw %}

##### eval

{% raw %}
```
;;; evalした結果を全部表示
(setq eval-expression-print-length nil)
```
{% endraw %}

##### 括弧

{% raw %}
```
;;; 対応する括弧を光らせる。
(show-paren-mode 1)
;;; ウィンドウ内に収まらないときだけ括弧内も光らせる。
(setq show-paren-style 'mixed)
```
{% endraw %}

昔はmic-paren.elも使っていましたが、標準の機能で十分なので、もう使っていません。

##### 空白

1年前は`show-trailing-whitespace`を使っていましたが、より多くの空白を視覚化できる`whitespace-mode`を使うようにしました。

{% raw %}
```
;; 2011-10-27
;; 空白や長すぎる行を視覚化する。
(require 'whitespace)
;; 1行が80桁を超えたら長すぎると判断する。
(setq whitespace-line-column 80)
(setq whitespace-style '(face              ; faceを使って視覚化する。
                         trailing          ; 行末の空白を対象とする。
                         lines-tail        ; 長すぎる行のうち
                                           ; whitespace-line-column以降のみを
                                           ; 対象とする。
                         space-before-tab  ; タブの前にあるスペースを対象とする。
                         space-after-tab)) ; タブの後にあるスペースを対象とする。
;; デフォルトで視覚化を有効にする。
(global-whitespace-mode 1)
```
{% endraw %}

##### 位置

{% raw %}
```
;;; 現在行を目立たせる
(global-hl-line-mode)

;;; カーソルの位置が何文字目かを表示する
(column-number-mode t)

;;; カーソルの位置が何行目かを表示する
(line-number-mode t)

;;; カーソルの場所を保存する
(require 'saveplace)
(setq-default save-place t)
```
{% endraw %}

##### 行

{% raw %}
```
;;; 行の先頭でC-kを一回押すだけで行全体を消去する
(setq kill-whole-line t)

;;; 最終行に必ず一行挿入する
(setq require-final-newline t)

;;; バッファの最後でnewlineで新規行を追加するのを禁止する
(setq next-line-add-newlines nil)
```
{% endraw %}

##### バックアップ

{% raw %}
```
;;; バックアップファイルを作らない
(setq backup-inhibited t)

;;; 終了時にオートセーブファイルを消す
(setq delete-auto-save-files t)
```
{% endraw %}

##### 補完

{% raw %}
```
;;; 補完時に大文字小文字を区別しない
(setq completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)

;;; 部分一致の補完機能を使う
;;; p-bでprint-bufferとか
(partial-completion-mode t)

;;; 補完可能なものを随時表示
;;; 少しうるさい
(icomplete-mode 1)
```
{% endraw %}

##### 履歴

{% raw %}
```
;;; 履歴数を大きくする
(setq history-length 10000)

;;; ミニバッファの履歴を保存する
(savehist-mode 1)

;;; 最近開いたファイルを保存する数を増やす
(setq recentf-max-saved-items 10000)
```
{% endraw %}

##### 圧縮

{% raw %}
```
;;; gzファイルも編集できるようにする
(auto-compression-mode t)
```
{% endraw %}

##### diff

{% raw %}
```
;;; ediffを1ウィンドウで実行
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;;; diffのオプション
(setq diff-switches '("-u" "-p" "-N"))
```
{% endraw %}

##### ディレクトリ

{% raw %}
```
;;; diredを便利にする
(require 'dired-x)

;;; diredから"r"でファイル名をインライン編集する
(require 'wdired)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
```
{% endraw %}

ファイル名をそのまま変更できるのは便利です。

##### バッファ名

{% raw %}
```
;;; ファイル名が重複していたらディレクトリ名を追加する。
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
```
{% endraw %}

##### 実行権

見直したら1年前の設定には抜けていたので追記しました。

ファイルの先頭に`#!...`があるファイルを保存すると実行権をつけます。

{% raw %}
```
;; 2012-03-15
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)
```
{% endraw %}

##### 大文字・小文字変換

`M-u`や`M-l`だけで十分なら必要ないでしょうが、たまに使いたくなるときがあるのです。

{% raw %}
```
;;; リージョンの大文字小文字変換を有効にする。
;; C-x C-u -> upcase
;; C-x C-l -> downcase
;; 2011-03-09
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
```
{% endraw %}

##### 関数名

ウィンドウの上部に現在の関数名を表示します。残念ながら大きい関数を編集しなければいけなくなったときに、今どこにいるかがわかりやすくなって便利です。

{% raw %}
```
;; 2011-03-15
(which-function-mode 1)
```
{% endraw %}

##### Emacsサーバー

ほとんどemacsclientは使いませんが、いつでもつながるようにはしています。

{% raw %}
```
;; emacsclientで接続できるようにする。
;; 2011-06-14
(server-start)
```
{% endraw %}

##### 追加の設定をロード

最後にconfig/以下に置いてある設定ファイルを読み込みます。`~/.emacs.d/config/local.el`があればそれも読み込みます。`local.el`はリポジトリに入っていないファイルです。このおすすめ設定を使う場合は`local.el`を自作してそこでカスタマイズしてください。

{% raw %}
```
;; 標準Elispの設定
(load "config/builtins")

;; 非標準Elispの設定
(load "config/packages")

;; 個別の設定があったら読み込む
;; 2012-02-15
(condition-case err
    (load "config/local")
  (error))
```
{% endraw %}

#### config/builtins.el: 標準Elispの設定

config/builtins.elには標準Elisp（Emacsに付属しているElisp）の設定を記述します。

##### バージョン管理システム

diredで"V"を入力するとそのディレクトリで使っているバージョン管理システム用のモードを起動します。1年前のものより賢く検出するようになっています。

{% raw %}
```
;; diredから適切なバージョン管理システムの*-statusを起動
(defun dired-vc-status (&rest args)
  (interactive)
  (let ((path (find-path-in-parents (dired-current-directory)
                                    '(".svn" ".git"))))
    (cond ((null path)
           (message "not version controlled."))
          ((string-match-p "\\.svn$" path)
           (svn-status (file-name-directory path)))
          ((string-match-p "\\.git$" path)
           (magit-status (file-name-directory path))))))
(define-key dired-mode-map "V" 'dired-vc-status)

;; directoryの中にbase-names内のパスが含まれていたらその絶対パスを返す。
;; 含まれていなかったらdirectoryの親のディレクトリを再帰的に探す。
;; 2011-03-19
(defun find-path-in-parents (directory base-names)
  (or (find-if 'file-exists-p
               (mapcar (lambda (base-name)
                         (concat directory base-name))
                       base-names))
      (if (string= directory "/")
          nil
        (let ((parent-directory (substring directory 0 -1)))
          (find-path-in-parents parent-directory base-names)))))
```
{% endraw %}

##### スペルチェック

自動でスペルチェックを実行します。スペルミスの単語は色が変わるのですぐに気づけます。

{% raw %}
```
;; 2011-03-09
(setq-default flyspell-mode t)
(setq ispell-dictionary "american")
```
{% endraw %}

##### text-mode

テキスト編集用のモード共通の設定です。

{% raw %}
```
;; 2012-03-18
;; text-modeでバッファーを開いたときに行う設定
(add-hook
 'text-mode-hook
 (lambda ()
   ;; 自動で長過ぎる行を分割する
   (auto-fill-mode 1)))
```
{% endraw %}

##### cc-mode

C言語と同じような構文のプログラミング言語用の設定です。

{% raw %}
```
;; 2012-03-18
;; c-modeやc++-modeなどcc-modeベースのモード共通の設定
(add-hook
 'c-mode-common-hook
 (lambda ()
   ;; BSDスタイルをベースにする
   (c-set-style "bsd")

   ;; スペースでインデントをする
   (setq indent-tabs-mode nil)

   ;; インデント幅を2にする
   (setq c-basic-offset 2)

   ;; 自動改行（auto-new-line）と
   ;; 連続する空白の一括削除（hungry-delete）を
   ;; 有効にする
   (c-toggle-auto-hungry-state 1)

   ;; CamelCaseの語でも単語単位に分解して編集する
   ;; GtkWindow         => Gtk Window
   ;; EmacsFrameClass   => Emacs Frame Class
   ;; NSGraphicsContext => NS Graphics Context
   (subword-mode 1)))
```
{% endraw %}

##### emacs-lisp-mode

Elispを編集するときの設定です。

{% raw %}
```
;; 2012-03-18
;; emacs-lisp-modeでバッファーを開いたときに行う設定
(add-hook
 'emacs-lisp-mode-hook
 (lambda ()
   ;; スペースでインデントをする
   (setq indent-tabs-mode nil)))
```
{% endraw %}

##### 追加の設定をロード

最後に`~/.emacs.d/config/buitins/local.el`があればそれもそれも読み込みます。`local.el`はリポジトリに入っていないファイルです。このおすすめ設定を使う場合は`local.el`を自作してそこでカスタマイズしてください。

{% raw %}
```
;; 個別の設定があったら読み込む
;; 2012-03-18
(condition-case err
    (load "config/builtins/local")
  (error))
```
{% endraw %}

#### config/packages.el: 非標準Elispの設定

##### el-get

パッケージ管理ステムとして複数のソースからパッケージをインストールできるel-getを使います。el-getがない場合は自動でインストールします。

{% raw %}
```
;; 2012-03-15
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(unless (require 'el-get nil t)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (end-of-buffer)
    (eval-print-last-sexp)))
;; レシピ置き場
(add-to-list 'el-get-recipe-path
             (concat (file-name-directory load-file-name) "/el-get/recipes"))
;; 追加のレシピ置き場
(add-to-list 'el-get-recipe-path
             "~/.emacs.d/config/el-get/local-recipes")
```
{% endraw %}

レシピは`~/.emacs.d/config/el-get/recipies/`に置いています。レシピを追加したい場合は`~/.emacs.d/config/el-get/local-recipies/`ディレクトリを作ってその下に`*.rcp`というファイルを作ってください。

##### grep-edit: grep結果をインラインで編集

grepの結果を直接編集できるようになります。wdiredと合わせてC-c C-cでも編集結果を反映できるようにしています。

{% raw %}
```
;;; *grep*で編集できるようにする
(el-get 'sync '(grep-edit))
(add-hook 'grep-setup-hook
          (lambda ()
            (define-key grep-mode-map
              (kbd "C-c C-c") 'grep-edit-finish-edit)))
```
{% endraw %}

##### Auto Complete: 自動補完

自動で補完候補をだしてくれて便利です。補完候補を`C-n`/`C-p`でも選択できるようにしています。

{% raw %}
```
;;; 自動補完
(el-get 'sync '(auto-complete))
(add-hook 'auto-complete-mode-hook
          (lambda ()
            (define-key ac-completing-map (kbd "C-n") 'ac-next)
            (define-key ac-completing-map (kbd "C-p") 'ac-previous)))
```
{% endraw %}

##### Anything

いろいろ便利に使えるらしいAnythingですが、`iswitchb-mode`と`yank-pop`の代わりにだけ使っています。`imenu`の代わりにも使ってみようとしています。

{% raw %}
```
;;; Anything
(let ((original-browse-url-browser-function browse-url-browser-function))
  (el-get 'sync '(anything))
  (require 'anything-config)
  (anything-set-anything-command-map-prefix-key
   'anything-command-map-prefix-key "C-c C-<SPC>")
  (define-key global-map (kbd "C-x b") 'anything-for-files)
  (define-key global-map (kbd "C-x g") 'anything-imenu) ; experimental
  (define-key global-map (kbd "M-y") 'anything-show-kill-ring)
  (define-key anything-map (kbd "C-z") nil)
  (define-key anything-map (kbd "C-l") 'anything-execute-persistent-action)
  (define-key anything-map (kbd "C-o") nil)
  (define-key anything-map (kbd "C-M-n") 'anything-next-source)
  (define-key anything-map (kbd "C-M-p") 'anything-previous-source)
  (setq browse-url-browser-function original-browse-url-browser-function))
```
{% endraw %}

##### Migemo: ローマ字で日本語をインクリメンタルサーチ

{% raw %}
```
;; 2012-03-19
;; インストールされていたら有効にする。
(require 'migemo nil t)
```
{% endraw %}

##### ruby-mode: Ruby編集用モード

Emacsに添付されているruby-modeは古いのでRubyのリポジトリに入っているものを使います。Emacsに添付されているruby-modeでは、`C-c C-e`で`end`を挿入することができなかったりします。

{% raw %}
```
;; 2012-03-15
(el-get 'sync '(ruby-mode-trunk))
```
{% endraw %}

##### rabbit-mode: Rabbitスライド編集用モード

[Rabbit](http://rabbit-shockers.org/)のスライドを編集するためのモードです。

{% raw %}
```
;; 2012-03-16
(el-get 'sync '(rabbit-mode))
```
{% endraw %}

##### run-test: テスト実行

`C-x C-t`で近くにあるrun-test.shやrun-test.rbという名前のファイルを実行するツールです。

{% raw %}
```
;;; テスト実行
(el-get 'sync '(run-test))
```
{% endraw %}

##### 追加の設定をロード

最後に`~/.emacs.d/config/packages/local.el`があればそれもそれも読み込みます。`local.el`はリポジトリに入っていないファイルです。このおすすめ設定を使う場合は`local.el`を自作してそこでカスタマイズしてください。

{% raw %}
```
;; 個別の設定があったら読み込む
;; 2012-03-15
(condition-case err
    (load "config/packages/local")
  (error))
```
{% endraw %}

### まとめ

Emacs実践入門と1年経ったおすすめのEmacsの設定を紹介しました。

ここで紹介した内容は[GitHub](https://github.com/clear-code/emacs.d)に置いておいたので、興味がある人は試してみてください。使い方はREADMEを参照してください。ここで紹介した内容が難しい場合はEmacs実践入門を読んでみるとよいかもしれません。

[https://amazon.co.jp/dp/9784774150024](https://amazon.co.jp/dp/9784774150024)

[^0]: 60ページの注2。
