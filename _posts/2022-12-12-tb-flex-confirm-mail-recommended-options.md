---
title: FlexConfirmMail（Thunderbird版）の推奨設定
author: piro_or
tags:
- mozilla
- security
---

結城です。

[当社のメール誤送信対策製品「FlexConfirmMail」のThunderbirdアドオン版](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)は、契約顧客からのご要望に応えるための改修を重ねてきた経緯もあって、多数の設定項目があります。
そのため、尻込みして使うことをためらったり、有効な設定の仕方が分からずあまり妥当でない設定の仕方をしてしまったりするケースがあるようです。

当社では自社内においても、ThunderbirdとFlexConfirmMailの組み合わせでの使用を推奨しています。
この記事では、これらの導入手順と当社の自社内向けの推奨設定[^version]について解説します。

[^version]: Thunderbird 102およびFlexConfirmMail バージョン4.1.5時点での物。

<!--more-->

### はじめに：セットアップ

最初に、当社内での推奨メールクライアント環境（Thunderbird + FlexConfirmMail）のセットアップ手順を説明します。

1. 業務用の端末にThunderbirdをインストールします。
   * Windowsの場合は[公式サイト](https://www.thunderbird.net/ja/) からインストーラを入手し、実行します。
   * Linuxの場合は、`apt`や`dnf`を利用してインストールします。
2. Thunderbirdを起動して、メールアカウントの情報を入力します。
   * 自社のメールサーバーの接続情報を入力します。
3. アプリケーションメニューの「ツール」→「アドオンとテーマ」を開き、「FlexConfirmMail」を検索します。
4. 検索結果のうち、アドオン名が「FlexConfirmMail」且つ作者が「ClearCode Inc.」である物をインストールします。  
   ![（Thunderbird Add-onsのアドオン検索結果一覧に表示されたFlexConfirmMailの項目の、インストールボタンをクリックする様子）]({% link images/blog/tb-flex-confirm-mail-recommended-options/install.png %})
5. FlexConfirmMailの設定画面を開きます。  
   ![（Thunderbirdのアドオン管理画面において、FlexConfirmMailの項目の設定ボタン＝スパナアイコンのボタンをクリックする様子）]({% link images/blog/tb-flex-confirm-mail-recommended-options/config.png %})

FlexConfirmMailの設定画面では次の一覧表の設定を入力します。

<table>
<tr><th>見出し</th><th>項目</th><th>推奨値</th></tr>
<tbody>
  <tr><td rowspan=8>宛先</td><td>送信前の宛先確認</td><td>誤送信の危険性がある場合だけ確認</td></tr>
  <tr><td>〜文字よりも長い文字列をペーストした時</td><td>100</td></tr>
  <tr><td>組織内として扱うメールアドレスのドメイン名</td><td>clear-code.com</td></tr>
  <tr><td>すべての宛先が登録済みドメインのメール送信時は確認ダイアログを表示しない</td><td>ON</td></tr>
  <tr><td>宛先が～件以下の場合は確認ダイアログを表示しない</td><td>0 (常に確認)</td></tr>
  <tr><td>ToまたはCcの宛先に～個以上のドメインが含まれる場合に警告する</td><td>OFF</td></tr>
  <tr><td>組織内の宛先の一括チェックを許可</td><td>ON</td></tr>
  <tr><td>外部の宛先の一括チェックを許可</td><td>OFF</td></tr>
  <tr><td rowspan=1>添付ファイル</td><td>添付ファイルの確認を求める</td><td>ON</td></tr>
  <tr><td rowspan=5>その他</td><td>送信ボタンを押してからカウントダウン後に実際に送信する</td><td>ON</td></tr>
  <tr><td>カウントダウンの秒数</td><td>5</td></tr>
  <tr><td>カウントを飛ばしてすぐに送信することを許容する</td><td>ON</td></tr>
  <tr><td>宛先の種別（To/Cc/Bcc）を強調表示する</td><td>ON</td></tr>
  <tr><td>それ以外の項目</td><td>OFF</td></tr>
</tbody>
</table>

すべての項目を入力したらセットアップ完了です。

### 解説：設定の背景と方針

FlexConfirmMailは、メール作成ウィンドウで「送信」ボタンを押した時に確認のためのダイアログを表示し、ダイアログ上で何らかの操作を行ってからの送信を求めるようにして、メールの誤送信を防止するアドオンです。
それぞれの設定項目は、大まかに以下のどちらかのカテゴリに分類できます。

* *確認条件*……確認ダイアログを表示する条件の指定
* *確認内容*……確認ダイアログ内で実際に確認を行う内容やUIの動作の指定

一般的に、誤操作を防ぐための「操作の実行時に確認を求める」UIは、誤操作防止の決定打とは*ならない*と言われることが多いです。
筆者の個人的な体験としても、何度も繰り返し同じ確認を目にすると、「ああ、またこの確認か」と思考停止してノーチェックで「OK」ボタンを押してしまう、[イソップ童話のオオカミ少年](https://ja.wikipedia.org/wiki/%E5%98%98%E3%82%92%E3%81%A4%E3%81%8F%E5%AD%90%E4%BE%9B)のような状態になりがちで、確認の頻度が高いほど、確認内容が煩わしいほど、その内容が定型的であるほど、その傾向は顕著になる印象があります。
*肝心な場面で確認が確認として機能しないにもかかわらず、手間だけは増える*のでは、百害あって一利なしと言わざるを得ません。

そのような経験則に基づいて、当社のFlexConfirmMailの運用は以下を基本的な方針としています。

* 確認条件……*誤送信の発生の危険性が高い場面や、誤送信発生後の被害が大きくなる恐れがある場面*に限定して確認を求めるようにして、そうでない場合は確認を求めない
* 確認内容……*宛先の間違いを防ぐ*ことのみに注力し、それ以外の確認は求めない

以下、この方針を前提に、個別の設定内容を説明していきます。

#### 「誤送信の危険性がある場合のみ確認する」について

FlexConfirmMailの初期設定は「常に確認する」ですが、当社では「誤送信の危険性がある場合のみ確認する」を推奨設定としています。

先に少し触れましたが、メールの送信時には、誤送信が発生しやすい場面とそうでない場面があります。
例えば、「機密情報をうっかり書いてしまった」といったヒューマンエラーが無い限り、以下のような場面では誤送信は起こりにくいでしょう。

* 受信したメールを閲覧中に返信ボタンを押し、本文を手入力して送信する。
* 既存の送信済みメールを選択して「新しいメッセージとして編集」を実行し、本文中の日付情報など細部だけを編集して送信する。

他方、以下の経緯でメールの誤送信が発生した事例は実際にあります。

* 既存のメールへの返信時に、Ccに無かったアドレスを手入力で追加しようとして、誤ったアドレスを追加して送信してしまった[^autocomplete]
* A社からのメールへの返信作成時に、B社からのメールの本文を誤ってコピー＆ペーストして送信してしまった
* A社からのメールへの返信内容について、長文であったためにテキストエディター上で編集していた物を、誤ってB社からのメールへの返信の本文としてペーストして送信してしまった

[^autocomplete]: 誤ったアドレスを手入力してしまう場合以外にも、アドレス帳に何らかの経緯で誤った宛先情報が保存されてしまっていて、オートコンプリートで本来の宛先ではなくそちらを選択してしまう、という場合もあります。

こういった誤送信の実例を分析した上で、それと同様の場面に該当していると判断できた場合に限定して送信前の確認を求めることで、オオカミ少年化による思考停止を防ごうという趣旨になります。

#### 組織内として扱うメールアドレスのドメイン名

この設定項目は、いわゆる「自社のメールアドレスのドメイン」を設定するための物です。

FlexConfirmMailは、確認ダイアログの「外部ドメインのメールアドレス」欄に、宛先のメールアドレスをドメインごとに整理して列挙するようになっています。
これにより、「A社宛のメールに誤って全く無関係のB社のメールアドレスが宛先として混入してしまった」といった事態が発生しても、送信前にそのことに気付きやすくなります。

しかしながら、この設定項目に指定されたドメインのメールアドレスの宛先だけは特別に、「登録済みドメインのメールアドレス」欄に列挙した上で、他の宛先に対しては許容されない「確認の省略」を、特別に許容できるようになっています。
複数のドメインを指定できるようになっているのは、グループ企業で使われている複数のドメインをすべて「社内」として扱って、グループ企業内とそれ以外とで異なるメール送信ポリシーを適用したい、というニーズがあるためです。
また、「社内」という表記でないのは、「会社」以外の任意団体で運用する場合や、同一社内であっても別部門には別ポリシーを適用したい場合などを想定しているためです。

#### ドメイン数に応じた警告

FlexConfirmMailでは、「宛先のドメインの数がN種類より多くある場合に警告する」という判断を行えます。
これは、A社のみ宛に送るべきメールをA社とB社の両方に対して送ってしまった、という経緯での誤送信発生リスクが疑われる場面で、確認を求めるようにする事を意図した機能です。

ただ、当社のお客さまとのやり取りでは、グループ企業の方の宛先が多数列挙されるケースが多いです。
この状況で「すべての宛先に返信」の操作をすると、毎回確認を求められることになってしまって、オオカミ少年化する懸念があります。
そのため当社では、宛先のドメインの数のみに着目した確認は特に行わない状態を推奨設定としています[^prevent-mixed-recipients]。

[^prevent-mixed-recipients]: 意図しない宛先の混入については、手動で宛先を追加したことをきっかけとした確認でカバーされるという考えです。

なお、類似する事故として、宛先の設定ミスによるメールアドレスの漏出[^bcc-trouble]が度々問題となることがあります。
現在のThunderbirdは、このような事故への対策として、異常に多く[^threshold]の宛先がToまたはCcに設定されている場合にBccの使用を推奨するよう促す機能が標準で含まれています。
Thunderbird 102以降のバージョンでは、設定エディターで `mail.compose.warn_public_recipients.aggressive` を `true` に設定しておくと、控えめな警告のみに留まらず、FlexConfirmMailのように操作をブロックするダイアログで再確認を求めるようになります。  
![（Thunderbirdによって表示される再確認のダイアログの様子。宛先同士に他の宛先が暴露されることが警告されている）]({% link images/blog/thunderbird-102/warn-too-many-public-recipients.png %})  
メールの一斉送信を行う機会が多い場合、この機能を使用するのもお勧めです。

[^bcc-trouble]: 互いに不可視となるBccではなく、互いに可視となるToまたはCcで大量の宛先を設定した状態でメールを一斉送信することで、個々の宛先に他のすべての宛先のメールアドレスが暴露されてしまう、という種類の事故。
[^threshold]: 具体的には、初期設定では15件が閾値となっています。閾値は、設定エディターで `mail.compose.warn_public_recipients.threshold` を変更して増減できます。

#### カウントダウン機能

カウントダウン機能は「メールの宛先確認」とは無関係の機能ですが、FlexConfirmMailの元になったThundebrirdアドオンから引き継いだ機能として残しています。
カウントダウンをキャンセルするとメール送信前の段階に戻れるため、送信確定後に立ち止まるチャンスが増えることになります。
[Gmailでは「送信操作確定後の一定時間メールの送信を保留しておき、後から気が変わった時に送信を取り消せる」といった機能が提供されています](https://support.google.com/mail/answer/2819488?hl=ja&co=GENIE.Platform%3DDesktop)が、その簡易版と言えます[^send-later]。

[^send-later]: Thunderbirdでは、「後で送信」を選択してメール送信操作を確定すると、ローカルフォルダーアカウントの「送信トレイ」にメールが保存され、「ファイル」メニューの「未送信メッセージを送信」を押すまで実際には送信しないという運用が可能です。しかしながら、手動で「未送信メッセージを送信」の操作をするまではメールが送信されず「送信トレイ」に残り続けてしまう（一定時間経過での自動送信はしてくれない）ため、Gmailの「送信後に一定時間内であれば取り消せるが、取り消さなければそのまま送信される」という運用の代替としては使いにくく、Gmailと同様の事をするには、[一定時間経過後などの条件で自動的にメールを送信する機能を提供するアドオン](https://addons.thunderbird.net/thunderbird/addon/send-later-3/)を使う必要があります。

このセクションには他にも、件名やメール本文の冒頭部分を確認ダイアログに表示するようにして、それらにもチェックを入れなければ送信できないようにする設定項目がありますが、当社ではいずれも無効の状態を推奨設定としています。
これも、オオカミ少年化の回避を意図してのことです。

#### 添付ファイル

当社では2022年12月現在、顧客へファイルを引き渡す場合には「自社運用のホスティングサービスにファイルを置き、ファイルのダウンロード用URLをメールで通知する」運用を推奨しています。
これは、*誤送信が発生した場合に、誤送信メールに記載されたダウンロード用URLを無効化することで、それ以後のファイルの拡散を防ぐ*ことを意図してのことです。

そのような背景があるため、メールにファイルを添付して送信するケースは当社の運用ではイレギュラーであり、特に注意が必要と考えられることから、添付ファイルがある場合には送信前の確認を行うことを推奨設定としています。

#### 番外編：誤字を防ぐには

誤送信対策という趣旨からは外れますが、著者はFlexConfirmMailを利用して、誤字（誤変換）が残ったままのメールの送信をなるべく防ぐようにしています。
具体的には次の設定で実現しています。

* `誤字チェック`と題した設定を以下の内容で追加し、ONに設定
  * マッチング対象：`件名と本文`
  * 対象のリスト：送信者の実際の過去の送信メールで発生した誤字のリストを指定
  * 条件に該当する場合の強調表示：`常に行う`
  * 条件に該当する場合：`常に再警告する`
  * 警告のメッセージ：
    ```
    件名または本文に誤字が疑われる以下の語句があります。
    
    %S
    
    本当に送信して宜しいですか？
    ```

警告対象とする語句は、個々人のキー入力の傾向やかな漢字変換の傾向などによって変わってきます。
筆者の場合は、本稿執筆時点では以下の語句を警告対象に設定しています。

```
すう。
ですう
ますう
ヘイ社
背手血
減少が
減少を
減少の
```

### まとめ

Thunderbird版FlexConfirmMailの設定について、当社内運用での推奨設定を参照しながら、オオカミ少年化しにくくする設定の考え方と、動作が分かりにくいと思われる設定項目の背景や動作をご紹介しました。

当社では、Thunderbirdの法人運用における誤送信対策の社内ポリシーの反映方法のご相談や、ポリシーの実現に必要なアドオンの開発・改修などを有償にて承っております。
Thunderbirdの運用でお悩みの企業のご担当者さまは、[お問い合わせフォームよりご相談ください]({% link contact/index.md %})。

また当社では、Thunderbird用と同様の事をMicrosoft Outlookでも実現する[Outlook用アドイン版FlexConfirmMail]({% post_url 2022-05-09-flexconfirmmail %})も開発・公開しています。
Outlookの法人運用において、特定の運用形態に合わせた機能追加を行うアドインを必要とされている企業のご担当者さまは、上記フォームよりお問い合わせ頂けましたら幸いです。
