---
tags: []
title: 'xml2po: XMLをgettextを使って国際化'
---
需要がないためか、Web上にあまり情報がないxml2poというツールについて紹介します。
<!--more-->


### xml2poとは

xml2poはXML内のテキスト情報を他の言語へ翻訳することを支援するツールです。ちなみにPythonで書かれています。

xml2poを使って英語のXML文書を日本語に翻訳する場合の流れは以下のようになります。

  1. 英語のXMLからテキスト部分を抽出する。

  1. テキストを翻訳する。

  1. 元の英語のXMLと翻訳済みのテキストを合わせて日本語のXMLを生成する。


図にすると以下のようになります。

![英語のXML文書を日本語に翻訳]({{ "/images/blog/20110824_0.png" | relative_url }} "英語のXML文書を日本語に翻訳")

### 翻訳方法

xml2poは翻訳のバックエンドに[Gettext](https://ja.wikipedia.org/wiki/Gettext)を採用しています[^0]。そのため、翻訳作業はGNU gettextを用いて翻訳する場合とほとんど同じようになります。GNU gettextを使ったことがある人にはxgettextの代わりにxml2poを使うといえばピンとくるかもしれません。

コマンドでいうと以下のような流れになります。

{% raw %}
```
% xml2po --mode xhtml --output hello.pot index.html
% msginit --input hello.pot --output ja.po --locale ja
% emacs ja.po
% xml2po --mode xhtml --po-file ja.po --language ja --output index.html.ja index.html
```
{% endraw %}

それぞれの作業を簡単な具体例を使ってみてみましょう。

### テキストを抽出

まず、翻訳元の英語のXHTMLを用意します。

{% raw %}
```html
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Hello</title>
  </head>

  <body>
    <h1>Hello</h1>
    <p>This is <em>English</em> XHTML.</p>
  </body>
</html>
```
{% endraw %}

このXHTMLからxml2poを使って翻訳対象のテキストを抽出します。

{% raw %}
```
% xml2po --mode xhtml --output hello.pot index.html
```
{% endraw %}

中身は以下のようになります。

{% raw %}
```
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-08-24 23:33+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: index.html:5(title) index.html:9(h1)
msgid "Hello"
msgstr ""

#: index.html:10(p)
msgid "This is <em>English</em> XHTML."
msgstr ""
```
{% endraw %}

このファイルを元に日本語用の翻訳ファイル（POファイル）を作ります。メールアドレスを聞かれるので入力します。

{% raw %}
```
% msginit --input hello.pot --output ja.po --locale ja
ユーザが翻訳に関するフィードバックをあなたに送ることができるように,
新しいメッセージカタログにはあなたの email アドレスを含めてください.
またこれは, 予期せぬ技術的な問題が発生した場合に管理者があなたに連絡が取れる
ようにするという目的もあります.

Is the following your email address?
  kou@localhost
Please confirm by pressing Return, or enter your email address.
kou@clear-code.com
http://translationproject.org/team/index.html を検索中... 完了.
A translation team for your language (ja) does not exist yet.
If you want to create a new translation team for ja, please visit
  http://www.iro.umontreal.ca/contrib/po/HTML/teams.html
  http://www.iro.umontreal.ca/contrib/po/HTML/leaders.html
  http://www.iro.umontreal.ca/contrib/po/HTML/index.html

ja.po を生成.
%
```
{% endraw %}

これで翻訳準備は完了です。

### 翻訳

次は、ja.poファイルを編集して英語のテキストを翻訳します。`msginit "..."`のテキストが翻訳対象の英語のテキストです。それに対応する翻訳後の日本語のテキストを`msgstr "..."`のところに入力します。

ja.po（変更前）:

{% raw %}
```
...
msgid "Hello"
msgstr ""
...
msgid "This is <em>English</em> XHTML."
msgstr ""
```
{% endraw %}

ja.po（変更後）:

{% raw %}
```
...
msgid "Hello"
msgstr "こんにちは"
...
msgid "This is <em>English</em> XHTML."
msgstr "これは<em>日本語</em>のXHTMLです。"
```
{% endraw %}

このPOファイルを便利に編集するためのツールがいくつかあるので、それを利用するとよいでしょう。例えば、Emacsにはpo-mode.elがあります。

### 国際化

翻訳ができたら、元の英語のXHTMLと翻訳したテキストを使って日本
語のXHTMLを生成します。

{% raw %}
```
% xml2po --mode xhtml --po-file ja.po --language ja --output index.html.ja index.html
```
{% endraw %}

生成されたXHTMLは以下のようになります。

{% raw %}
```html
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>こんにちは</title>
  </head>

  <body>
    <h1>こんにちは</h1>
    <p>これは<em>日本語</em>のXHTMLです。</p>
  </body>
</html>
```
{% endraw %}

きちんと翻訳されていますね。

### まとめ

XML文書を国際化するためのツールであるxml2poを紹介しました。xml2poを使えば、ドキュメントツールが国際化に対応していなくても、XHTMLを出力してくれさえすればドキュメントを国際化できます。つまり、[YARD](http://yardoc.org/)をドキュメントシステムとして使いながら国際化したドキュメントを生成できるということです。実際に[rroongaのリファレンスマニュアル](http://groonga.rubyforge.org/rroonga/ja/)がこの方法を採用しています。

XML文書を国際化したい場合はxml2poを使ってみてはいかがでしょうか。今回はXHTMLに対して使いましたが、[DocBook](https://ja.wikipedia.org/wiki/DocBook)形式のサポートの方が充実しているので、DocBookのファイルを国際化したい場合は有力な選択肢になるでしょう。

[^0]: もう少し言うと、GNU gettextのファイルフォーマット仕様と管理ツールとPythonで実装されたGNU gettextの仕様に対応したライブラリを用いる。
