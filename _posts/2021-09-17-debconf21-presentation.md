---
title: DebConf21でまだ手がつけられていないバグを見つけやすくする仕組みの最近の話をしました
author: kenhys
tags:
- presentation
---

[DebConf21](https://debconf21.debconf.org/)で[Latest topics about fabre.debian.net](https://debconf21.debconf.org/talks/16-latest-updates-about-fabredebiannet-finding-untouched-bugs/)という話をしてきた林です。今回はその発表内容を紹介します。

<!--more-->

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kenhys/debconf2021-online/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kenhys/debconf2021-online/" title="DebConf 2021 Online">DebConf 2021 Online</a>
  </div>
</div>

関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kenhys/debconf2021-online/)

### DebConf21について

DebConfとは、年に一回Debian開発者があつまるカンファレンスです。今年も昨年に引き続きオンラインでの開催となりました。
来年のDebConf22ではコソボでの開催を模索しているようですが、オフラインで開催できるかは未知数です。

### 発表内容について

昨年は、[DebConf20でバグトラッキングに関する発表をしました]({% post_url 2020-08-26-index %})という記事にもあるように、
従来のバグトラッキングシステムでカバーできないところを補完する実験的なシステムを構築し、その成果を発表しました。

今回の発表はその続きで、昨年のDebConf20からの更新内容をショートトーク(20分枠)で紹介しました。[^prerecord]

[^prerecord]: 当日のトラブルが怖かったので事前録画したものを配信してもらいました。が、当日質疑応答のときにマイクでトラブりました。

具体的には以下のトピックを紹介しました。

* debian.netのサブドメインとして運用することになったこと
* Debian.netチームの支援を受けるようになったこと
* fabre.debian.netを使うと便利な例について

最初のトピックはドメインの変更についてです。
実験的なシステムだったので、DebConf20の頃は個人のサブドメインで運用していました。
その後、[Debian開発者になった]({% post_url 2020-09-28-index %})こともあり、運用しているシステムをdebian.net配下のサブドメインへと変更しました。
現在は、https://fabre.debian.net として運用しています。
debian.orgのサブドメインだとDebian公式ですが、debian.netのサブドメインなのであくまでも非公式なサービスです。

Debian開発者になるとdebian.netのサブドメインを利用できる仕組みがあるのでそれを利用しています。
ほかにもサブドメインで運用されているものはいくつもあります。https://wiki.debian.org/DebianNetDomains をみると現在運用されているものを一覧できます。

2つ目のトピックは、スポンサーについてです。
実験的なシステムだったので、以前はConoHaの最小構成のVPSインスタンスを使っていました。
リソースの不足が常態化していたため、昨年[FOSSHOST](https://fosshost.org/)にVPSを提供してもらっていました。
リソース等には問題がなくなったのですが、今年になってFOSSHOSTとFreenodeとのパートナーシップが発表されるなど先行きが不鮮明[^freenode]になったこともあり、Debian.netチームの支援を受けることにしました。
現在は、Debian.netチームの支援を受けて[Hetzner](https://www.hetzner.com/)のVPSに移転して運用しています。

[^freenode]: 現在は https://fosshost.org/news/freenode-partnership-to-be-discontinued FOSSHOSTとFreenodeとのパートナーシップは解消されています。

最後のトピックはfabre.debian.netを使うと便利なケースについてです。
いくつかのカテゴリごとや重要度に分類してバグを見やすく一覧できる[^udd]ようにしたり、
まだ誰も手をつけていないバグのみを表示できるようになっていることを説明しました。

[^udd]: この点だけをみるとUltimate Debian Databaseの検索 https://udd.debian.org/bugs/ でもすでにできます。ただしもうすでに誰かが作業しているかどうかはわかりにくいです。これはUDDがバグレポートに対するコメントの有無まではトラッキングしないためです。

最近リリースされたDebian 11 (Bullseye)では[libappindicatorライブラリは提供されなくなりました](https://www.debian.org/releases/stable/amd64/release-notes/ch-information.ja.html#obsolescense-and-deprecation)が、
まだ移行が進んでいなかったパッケージを見つけては、パッチを送るとかupstreamにプルリクエストを送って取り込んでもらったりするのにこのシステムを実際に活用していました、というのは誰も知らない裏話だったりします。

### さいごに

今回は、DebConf21での発表内容を紹介しました。
https://fabre.debian.net はbugs.debian.orgを補完するシステムとして改善を続けていく予定です。
その成果は次回のDebConfで発表できるといいなと考えています。
