---
tags:
- fluentd
title: Fluent-plugin-geoipのメンテナになりました
---
[@y-ken](https://github.com/y-ken)さんからコミット権をもらって gem owner に加えてもらったので[fluent-plugin-geoip](https://github.com/y-ken/fluent-plugin-geoip) v1.0.0などをリリースしました。
<!--more-->


v1.1.0までの主な変更点は以下の通りです。

  * Fluentd v1のプラグインAPIに移行した

    * Fluentd v0.12.x については fluent-plugin-geoip 0.8.x でサポートする

  * geoip2_cがデフォルトのバックエンドとなった

  * ドキュメントを更新した

    * 主にfilter_geoipを使ってもらうために、filter_geoipの説明を前に移した

    * 設定の説明を追加した

  * latitude, longitude にはデフォルト値を設定するようにした

  * filter_geoipでtagやtimeなどをレコードに挿入できるようにした

    * out_geoipではサポートしていた機能

  * どんなキーを指定すればよいのか簡単に調べるためにgeoip,geoip2のデータを確認するためのスクリプトを追加した

    * GeoIP Legacyでは[GeoIP Legacy CSV Databases](https://dev.maxmind.com/geoip/legacy/csv/)で調べるように説明していましたが、GeoIP2では[GeoIP2 City and Country CSV Databases](https://dev.maxmind.com/geoip/geoip2/geoip2-city-country-csv-databases/)を参照しても正しいキーがわからないため、簡単に確認できるスクリプトを追加しました

GeoIP2の使い方は[以前の記事]({% post_url 2017-04-18-index %})や[README.md](https://github.com/y-ken/fluent-plugin-geoip#readme)を参照してください。

なお、[ダウンロード可能なGeoIP2のデータベースは複数あります](https://dev.maxmind.com/geoip/geoip2/downloadable/)が、geoip2_cは全てのデータベースを扱うことができるので、fluent-plugin-geoipでもバックエンドをgeoip2_cにしていれば全てのデータベースを扱うことができます。
しかし、fluent-plugin-geoipは1つのイベントに対して複数のデータベースを検索することはサポートしていないので、1つのイベントに対して複数のデータベースから取得した情報を付加したい場合は以下のように複数回フィルターを適用すればよいです。

```
<filter>
  @type geoip
  @id city
  geoip2_database /path/to/GeoLite2-City.mmdb
  <record>
    city ${city.names.en["host"]}
  </record>
</filter>
<filter>
  @type geoip
  @id asn
  geoip2_database /path/to/GeoLite2-ASN.mmdb
  <record>
    asn_id ${autonomous_system_number["host"]}
    asn_organization ${autonomous_system_organization["host"]}
  </record>
</filter>
```


```
2017-12-25 12:33:34.019899208 +0900 raw.dummy: {"host":"66.102.9.80","message":"test","city":"Mountain View","asn_id":15169,"asn_organization":"Google LLC"}
2017-12-25 12:33:35.021822991 +0900 raw.dummy: {"host":"66.103.9.81","message":"test","city":"Hollywood","asn_id":23089,"asn_organization":"Hotwire Communications"}
```


これはgeoip2_cを使うようになってからできることだったのですが、あまり知られていないようなので設定例と一緒に紹介しました。

### まとめ

fluent-plugin-geoip v1.0.0での主な変更点とできるようになったことを紹介しました。
