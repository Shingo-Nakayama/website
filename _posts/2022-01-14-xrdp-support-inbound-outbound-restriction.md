---
title: xrdpでクリップボードの挙動をより細かく制御できるようになりました
author: kenhys
tags:
  - use-case
---

[xrdp](http://xrdp.org/)はWindowsのリモートデスクトップ接続を使ってLinuxに接続できるようにするソフトウェアです。

今回は、[サイバートラスト株式会社様](https://www.cybertrust.co.jp/)からの発注を受けて開発した、クリップボードの挙動をより細かく制御する機能を紹介します。

<!--more-->

xrdpでは、クリップボードを経由してテキスト・画像・ファイルのコピーを双方向で行えます。
しかし、企業で利用する場合には、容易にデータを持ち出せてしまうので、この機能が問題となることがあります。
そこで、要件に応じてより細かくクリップボードの挙動を制御できるようにする必要がありました。

### これまでのxrdpでできていたこと

従来のxrdp (0.9.18まで)でも、クリップボードの機能を制限する仕組みはありました。
それが、`/etc/xrdp/sesman.ini`の`RestrictOutboundClipboard`という設定です。

`[Security]`セクションの`RestrictOutboundClipboard`には次のように真偽値を指定できます。

```
[Security]

# Linux(xrdp)からWindowsへクリップボードを経由したコピーを許可する(既定値)
RestrictOutboundClipboard=false

# Linux(xrdp)からWindowsへクリップボードを経由したコピーを禁止する
RestrictOutboundClipboard=true
```

ただし、`RestrictOutboundClipboard`とあるように、できることはLinux(xrdp)からWindowsへクリップボードを経由したコピーの禁止です。
そして、`RestrictOutboundClipboard`では、テキスト・画像・ファイルの単位で個別にコピーを禁止できませんでした。
Windowsからクリップボードを経由してテキスト・画像・ファイルをLinux(xrdp)へと持ち出すことも制限できません。[^disable]

[^disable]: `/etc/xrdp/xrdp.ini`で`cliprdr=false`でクリップボードの共有自体を無効化するという手もありますが、細かく制御したいという要求は満たせません。

### xrdp 0.9.19でできるようになること

新しいバージョンのxrdp 0.9.19では次のことができるようになります。

<i class="fa fa-info-circle"></i> 2022/3/18追記: 2022/3/17日にxrdp 0.9.19として本機能がリリースされました。

* Linux(xrdp)からWindowsへのクリップボードのコピー機能の制限強化
  * クリップボードを経由したテキストのみコピーを禁止する
  * クリップボードを経由した画像のみコピーを禁止する
  * クリップボードを経由したファイルのみコピーを禁止する
* WindowsからLinux(xrdp)へのクリップボードのコピー機能の制限
  * クリップボードを経由したテキスト・画像・ファイルのコピーを一括して禁止する
  * クリップボードを経由したテキストのみコピーを禁止する
  * クリップボードを経由した画像のみコピーを禁止する
  * クリップボードを経由したファイルのみコピーを禁止する

上記の機能制限は、複数を組み合わせて指定できるようになります。

機能制限を設定で変えられるように、設定パラメータである`RestrictInboundClipboard`を追加し、`RestrictOutboundClipboard`の仕様を拡張しました。


`/etc/xrdp/sesman.ini`の`[Security]`セクションに設定するそれぞれの書式は次のとおりです。

従来の設定の書式は次のようになっていました。

```
RestrictOutboundClipboard=false|true
```

新しいxrdpでは次のようになります。

```
RestrictInboundClipboard=none|text|image|file|all|もしくはそれらの組み合わせ
RestrictOutboundClipboard=none|text|image|file|all|もしくはそれらの組み合わせ
```

`none`は制限しないことを意味します。`all`はテキスト・画像・ファイルを制限することを意味します。

従来との互換性の観点から`RestrictInboundClipboard`や`RestrictOutboundClipboard`に真偽値[^compatibility]も使えますが、非推奨です。

[^compatibility]: `false`は`none`と同じ、`true`は`all`と同じ意味として解釈される。

例えば次のような設定が行えます。

```
[Security]

# Linux(xrdp)からWindowsへクリップボードを経由したテキストのコピーを禁止する場合
RestrictOutboundClipboard=text

# Linux(xrdp)からWindowsへクリップボードを経由した画像のコピーを禁止する場合
RestrictOutboundClipboard=image

# Linux(xrdp)からWindowsへクリップボードを経由したファイルのコピーを禁止する場合
RestrictOutboundClipboard=file

# Linux(xrdp)からWindowsへクリップボードを経由したテキスト・画像・ファイルのコピーを禁止する場合
RestrictOutboundClipboard=all

# Linux(xrdp)からWindowsへクリップボードを経由したテキスト・ファイルのコピーを禁止する場合
RestrictOutboundClipboard=text, file

# WindowsからLinux(xrdp)へクリップボードを経由したテキストのコピーを禁止する場合
RestrictInboundClipboard=text

# WindowsからLinux(xrdp)へクリップボードを経由した画像のコピーを禁止する場合
RestrictInboundClipboard=image

# WindowsからLinux(xrdp)へクリップボードを経由したファイルのコピーを禁止する場合
RestrictInboundClipboard=file

# WindowsからLinux(xrdp)へクリップボードを経由したテキスト・ファイルのコピーを禁止する場合
RestrictInboundClipboard=text, file

# WindowsからLinux(xrdp)へクリップボードを経由したテキスト・画像・ファイルのコピーを禁止する場合
RestrictInboundClipboard=all
```


### 成果をアップストリームにフィードバックしよう

今回の依頼内容は、xrdpでクリップボードの機能をより細かく制御できるようにすることでした。
そこで、既存の`RestrictOutboundClipboard`の仕様を拡張したパッチを作成することで実現しました。
しかし、開発したパッチの内容自体は特殊な用途ではなく、汎用的なものです。
この成果はできればアップストリームにフィードバックしたいと考えました。

フィードバックすることのメリットは次のとおりです。

* よりよい実装にするためのフィードバックをもらえる
* コミュニティーにフィードバックすることで、似たような機能をバラバラに開発する必要がなくなる
* 将来的なバージョンアップにともない、自社でパッチのメンテナンスするコストを下げられる

一方でフィードバックしないことによるデメリットは次のとおりです。

* 将来のバージョンアップにともないパッチ自体をメンテナンスしていくコストがかかる
  * 実装が変更されたらそれに追従していかないといけない

クリアコードでは、フリーソフトウェアとビジネスの両立を理念としています。
発注元であるサイバートラスト様にもフィードバックすることによるメリットがあるので、
その旨を説明して開発した成果をアップストリームにフィードバックすることを許可していただきました。


実際にフィードバックしたPRは次のとおりです。

* [Extend inbound/outbound clipboard restriction](https://github.com/neutrinolabs/xrdp/pull/2082)

### 成果をフィードバックしたことでよかったこと

初期のパッチでは、機能を制限するために複数の設定パラメータを導入していました。

これは、xrdpの既存の設定ファイルのパーサーの実装が真偽値のみに対応していたためです。

```
RestrictInboundClipboardText=...
RestrictInboundClipboardImage=...
RestrictInboundClipboardFile=...
```

アップストリームに今回の成果をフィードバックした結果、テキスト・画像・ファイルごとに設定パラメータを増やすのではなく、複数の設定値を指定できるようにすべきとの意見がありました。
そのため、設定ファイルのパーサーにその実装を追加しました。
この部分も汎用的な機能です。
今後なんらかの機能追加がなされたときに、複数の設定値を受け付ける設定パラメータの追加が簡単にできるようになりました。

これらはアップストリームにフィードバックしていたことで、よりよい実装にできた事例と言えます。

### まとめ

今回は、[サイバートラスト株式会社様](https://www.cybertrust.co.jp/)からの発注を受けて開発した、クリップボードの挙動をより細かく制御する機能を紹介しました。
xrdpの0.9.19[^schedule]からはこの機能が使えるようになります。

[^schedule]: ~~直近のリリースの間隔からすると、おおよそ4ヶ月後とか半年後くらいに0.9.19がでるのではないかと予想。~~ 初稿の時点ではリリース未定でしたが、0.9.19が3/17にリリースされたため訂正しました。
