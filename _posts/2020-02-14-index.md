---
tags:
- fluentd
title: FluentdとそのプラグインのWindowsでの開発環境
---
### はじめに

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加しています。
<!--more-->


また、Fluentdにはプラグインというしくみがあり、たくさんのプラグインが開発されています。
FluentdはWindowsでも動くようになっています。FluentdやそのプラグインはmacOSやLinux上で開発するのが一般的であり、Windowsでプラグインを開発するときの開発環境についての記事を見かけないため、筆者畑ケの環境をまとめてみます。

### Fluentdやそのプラグインの開発環境

筆者はFluentdやそのプラグインの開発をするときにWindows環境を使う時があります。
Windows 10 Pro 1909環境で、少しRubyのバージョンが古めですが、2.5.7を使用しています。
このRubyは[RubyInstaller2](https://github.com/oneclick/rubyinstaller2)のプロジェクトが公開しているインストーラーです。

シェル環境は非Windows環境と*統一していません*。
ターミナルエミュレータには[Windows Terminal (Preview)](https://github.com/microsoft/terminal)を使用しています。
Windows Terminal (Preview)はv0.4.2382.0辺りのころから使用しています。
そこで動かすシェルは[PowerShell](https://github.com/PowerShell/PowerShell)を使用しています。執筆時点ではPowerShell 6.2.4を動かしています。
PowerShellを使っている理由は筆者畑ケが.NET環境の言語やライブラリに馴染みがあるからです。
RubyInstaller2からは[MSYS2](https://www.msys2.org/)というWindows環境でgccを用いた開発環境を作成できるツールチェインがあるため、これをRubyの拡張ライブラリのビルドに使用しています。

GitはWindows用のインストーラーを[Git for Windows](https://gitforwindows.org/)が用意しているのでこれを使用します。

PowerShellには[posh-git](https://github.com/dahlbyk/posh-git)を入れてgitのbranchの様子などを表示させています。

また、[Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/)を使用し、Dockerコンテナを動かす必要があるときはこのパッケージ由来のdockerコマンドを使っています。

Docker Desktop for WindowsのdockerコマンドはWindows環境ではコマンドプロンプトやPowerShellでの動作が想定されており、それ以外のシェルでの動作が想定されていないようです。

エディタは[GNU Emacs](https://www.gnu.org/software/emacs/)または[Visual Studio Code](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)を使用しています。
エディタに関しては普段使っている環境との乖離を少なくするため、非Windows環境と共通のGNU Emacsを使っていることが多いです。

#### まとめ

FluentdやそのプラグインのWindows開発環境を解説してみました。
WindowsではPowerShell環境が.NETと親和性が高く、スクリプトとしても痒いところに手が届くようになっています。
そのため、筆者はMSYS2付属のbashやコマンドプロンプトを使わずにPowerShellをWindowsでは普段使いのシェル環境としています。
