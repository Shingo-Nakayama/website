---
tags:
- ruby
title: るびま0026号
---
先日、[るびま0026号](http://jp.rubyist.net/magazine/?0026)がリリースされました。
<!--more-->


今回はRegionalRubyKaigi特集のようで、5Kaigiのレポートが載っています。5Kaigiのうち、[仙台Ruby会議01に参加]({% post_url 2009-01-26-index %})しましたが、そのレポートもあります。

### 仙台Ruby会議01

[仙台Ruby会議01のレポート](http://jp.rubyist.net/magazine/?0026-SendaiRubyKaigi01Report)は[id:monyakata](http://d.hatena.ne.jp/monyakata/)さんが丁寧にすっきりとまとめてくれています。とても読みやすいので参加できなかった人は読んでみてはいかがでしょうか。

レポートによると[まず好きなこと、そしてそれを続けること](/archives/SendaiRubyKaigi01/)のセッションが一番参加者が多かったそうです。参加してくれた皆さん、関係者の皆さん、ありがとうございました。

レポート中にある通り、[仙台Ruby会議01のサイト](http://regional.rubykaigi.org/sendai01)には東北・仙台情報が豊富にあります。もし、レポートを読んで仙台を感じたくなったときはサイトにある情報が役に立つことでしょう。お菓子やたいやき情報もあるので、甘いものが好きな人はぜひ参考にしてください。

### RubyNews

あまり話題にのぼらない感がある常設コーナーのRubyNewsですが、毎回充実しています。読むと、Ruby界隈では思ったよりいろんなことがあったなぁと感じることができます。[今回のRubyNews](http://jp.rubyist.net/magazine/?0026-RubyNews)はいつも以上に充実しています。読んでみると知らないことも多いかもしれません。

最近は[助田さんがruby-talkでリリースアナウンスのあったソフトウェアを紹介](http://suke.cocolog-nifty.com/blog/)していて、海外のRuby関連ソフトウェアに関する情報源としてとても貴重です。実は、RubyNewsにはruby-listでリリースアナウンスのあったソフトウェアが載っているので、日本でのRuby関連ソフトウェアのとても貴重な情報源になっています。

もちろん、[肉リリースされたRabbit]({% post_url 2009-02-10-index %})の情報も[載っています](http://jp.rubyist.net/magazine/?0026-RubyNews#l2)し、[RSS Parserのリリース](http://jp.rubyist.net/magazine/?0026-RubyNews#l35)も載っています。ソフトウェアをリリースしたときはruby-listでもリリースアナウンスをしてみてはいかがでしょうか。きっとRubyNewsに載せてもらえます。

### まとめ

るびま0026号がリリースされました。るびまは今回のRegionalRubyKaigiレポートのように毎号変わるコーナーにも有用な記事が多くありますが、実は常設コーナーの中にも有用な情報があります。常設コーナーを読み飛ばしている方はもったいないですよ。

[高橋編集長の巻頭言](http://jp.rubyist.net/magazine/?0026-ForeWord)はよく読まれていると思いますが、[編集後記](http://jp.rubyist.net/magazine/?0026-EditorsNote)もわりとおもしろいですよ。

仙台Ruby会議01レポートだけではなく、[zundaさん](http://zunda.freeshell.org/d/)編集のRubyNewsも紹介してみました。
