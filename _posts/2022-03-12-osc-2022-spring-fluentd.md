---
title: "OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報についてOSC 2022 Online Springで発表しました"
author: kenhys
tags:
- fluentd
- presentation
---

林です。

2022年3月11日・12日に開催された[Open Source Conference 2022 Online Spring](https://event.ospn.jp/osc2022-online-spring/)において、「OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報」と題した発表を行いました。
二日目のD会場にて実施した発表内容を紹介します。

<!--more-->

## 発表資料について


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kenhys/osc2022-online-spring-fluentd/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kenhys/osc2022-online-spring-fluentd/" title="OSC 2022 Online Spring Fluentd">OSC 2022 Online Spring Fluentd</a>
  </div>
</div>

* [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/kenhys/osc2022-online-spring-fluentd/)
* [リポジトリー](https://github.com/kenhys/rabbit-slide-kenhys-osc2022-online-spring-fluentd)

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/ashie/osc2022-online-spring-fluentd/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/ashie/osc2022-online-spring-fluentd/" title="OSC 2022 Online Spring Fluentd">OSC 2022 Online Spring Fluentd</a>
  </div>
</div>

* [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/ashie/osc2022-online-spring-fluentd/)
* [リポジトリー](https://gitlab.com/ashie/rabbit-slide-ashie-osc2022-online-spring-fluentd)


## 発表内容について

Fluentdに関して、日本語での情報発信があまりできていなかったり、日本からの開発参加が少ない印象がありました。
そこで、クリアコードが開発に参加するようになってからのできごとなどをOSCで発表することにしました。

前半は、プロジェクトの開発体制が変わったことに関連し、今後メンテナンスを継続していくために工夫したことを紹介しました。
Fluentd特有の話もありますが、GitHub Issueの運用のトピックについてはほかのOSSでも活用できるはずです。

後半は、主にプロジェクト運営の観点からFluentdの最新情報について紹介しました。
直近リリースされたバージョンのまとめや最新情報の入手方法、今後の開発予定など、まだアイデア段階のものまで多岐にわたる内容となっています。


当日の発表の内容はYoutubeのOSPN.jpチャンネルのアーカイブで視聴できます。

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/pLavhtPprnw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## まとめ

OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報と題してFluentdを継続的にメンテナンスしていけるように工夫したことや、主に運営面の観点からFluentdの最新情報について発表したことを紹介しました。

OSCでは、あくまでコミュニティーとしての活動内容の話をすることが目的なので言及していませんでしたが、クリアコードでは、[Fluentdのサポートサービス]({% link services/fluentd.md %})を提供しています。サポートサービスでは障害発生時の調査や回避策の提案、パッチの提供などを行います。
Fluentdに関するトラブルを抱えて困っている方は、ぜひこちらの[お問い合わせフォーム]({% link contact/index.md %})からご連絡ください。
