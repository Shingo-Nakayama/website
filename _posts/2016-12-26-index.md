---
tags: []
title: Debian GNU/LinuxでCMakeを使ってWindows用バイナリーをビルドする方法
---
2016-12-26時点でのDebian GNU/Linux sidでの話です。
<!--more-->


以前、[GNU Autotoolsを使ってWindows用バイナリーをビルドする方法]({% post_url 2011-10-13-index %})を紹介しましたが、ここで紹介するのはCMakeを使う方法です。

CMakeでクロスコンパイルする方法は[cmake-toolchains(7)](https://cmake.org/cmake/help/v3.7/manual/cmake-toolchains.7.html)に説明があります。CMakeはクロスコンパイル用に[`CMAKE_TOOLCHAIN_FILE`](https://cmake.org/cmake/help/v3.7/variable/CMAKE_TOOLCHAIN_FILE.html)という変数を用意しています。前述のドキュメントはこの変数を使う方法を説明しています。

ただ、ファイルを作るのは少し面倒なので、ここではコマンドラインオプションでWindows用バイナリーをビルドする方法を紹介します。

### 用意するもの

Debian GNU/Linux上でWindows用バイナリをビルドするためにはクロスコンパイルする必要があります。そのためにMinGW-w64を使います。MinGW-w64はGCCでWindows用バイナリをビルドするために必要なヘッダーファイルやライブラリなど一式を提供します。

cmakeパッケージとmingw-w64パッケージをインストールします。wineパッケージは動作確認用です。

```text
% sudo apt install -V cmake mingw-w64 wine
```


準備はこれで完了です。

### ビルド

ここではCMakeでのビルドに対応している[Groonga](http://groonga.org/ja/)をクロスコンパイルします。

まず、ソースコードをダウンロードして展開します。

```text
% wget http://packages.groonga.org/source/groonga/groonga-6.1.1.tar.gz
% tar xvf groonga-6.1.1.tar.gz
% cd groonga-6.1.1
```


CMakeを使うときはソースコードのあるディレクトリーではなく別のディレクトリーをビルドディレクトリーにすることが多いですが、ここでは説明を簡単にするためにソースコードのあるディレクトリーを使います。

GNU Autotoolsのときは`--host=x86_64-w64-mingw32`を指定するだけであとはいい感じに設定してくれますが、CMakeは次のようにいくつかのパラメーターを指定します。この中でクロスコンパイルに直接関係ないパラメーターは`CMAKE_INSTALL_PREFIX`だけです。これはインストール先を変更するために指定しているだけです。

```text
% PKG_CONFIG_LIBDIR=/tmp/local.windows/lib/pkgconfig \
    cmake \
      -DCMAKE_INSTALL_PREFIX=/tmp/local.windows \
      -DCMAKE_SYSTEM_NAME=Windows \
      -DCMAKE_SYSTEM_PROCESSOR=x64 \
      -DCMAKE_C_COMPILER=x86_64-w64-mingw32-gcc \
      -DCMAKE_CXX_COMPILER=x86_64-w64-mingw32-g++ \
      -DCMAKE_RC_COMPILER=x86_64-w64-mingw32-windres
```


各パラーメーターについて説明します。

  * `PKG_CONFIG_LIBDIR`: `pkg-config`が`.pc`ファイルを検索するときに使うパス。通常は（`LIBDIR`ではなく）`PKG_CONFIG_PATH`を指定するが、クロスコンパイルのときは`PKG_CONFIG_LIBDIR`を指定して、ビルド環境（この場合はDebian GNU/Linux）の`.pc`ファイルを一切検索しないようにする。

  * [`CMAKE_SYSTEM_NAME`](https://cmake.org/cmake/help/v3.7/variable/CMAKE_SYSTEM_NAME.html): `Windows`を指定するとCMakeに組み込まれているWindows用のビルドの設定を使ってくれる。（`/usr/share/cmake-3.7/Modules/Platform/Windows.cmake`などを使ってくれる。）`CMAKE_SYSTEM_NAME`を設定するときはあわせて[`CMAKE_SYSTEM_VERSION`](https://cmake.org/cmake/help/v3.7/variable/CMAKE_SYSTEM_VERSION.html)も設定するべきだが、MinGW-w64でクロスコンパイルするときは`CMAKE_SYSTEM_VERSION`を使っていなそうなので省略している。

  * [`CMAKE_SYSTEM_PROCESSOR`](https://cmake.org/cmake/help/v3.7/variable/CMAKE_SYSTEM_PROCESSOR.html): 32bitのバイナリーをビルドするか64bitのバイナリーをビルドするかを指定する。32bitなら`x86`で64bitなら`x64`になる。ここでは`x64`を指定しているので64bitのバイナリーをビルドする。

  * `CMAKE_C_COMPILER`: MinGW-w64が提供する`gcc`のコマンド名を指定する。`PATH`が通っていない場合はフルパスで指定する。

  * `CMAKE_CXX_COMPILER`: MinGW-w64が提供する`g++`のコマンド名を指定する。`PATH`が通っていない場合はフルパスで指定する。

  * `CMAKE_RC_COMPILER`: MinGW-w64が提供する`windres`のコマンド名を指定する。`PATH`が通っていない場合はフルパスで指定する。`.rc`ファイルを使わない場合は指定しなくても大丈夫。

[`CMAKE_FIND_ROOT_PATH_MODE_PROGRAM`](https://cmake.org/cmake/help/v3.7/variable/CMAKE_FIND_ROOT_PATH_MODE_PROGRAM.html)なども指定した方が間違ってビルド環境にあるプログラムやライブラリーなどを見つけてしまわなくてよいのですが、Groongaのように`pkg-config`を使っている場合は設定しなくても大丈夫です。

あとは`make`でビルドできます。

```text
% make
```


`make install`すると`CMAKE_INSTALL_PREFIX`で指定したディレクトリー以下にインストールされます。

```text
% make install
```


[Wine](https://www.winehq.org/)を使うとDebian GNU/Linux上でインストールしたバイナリーで動作確認できます。Groongaのように`g++`を利用している場合は`libstdc++-6.dll`と`libgcc_s_seh-1`（`seh`はStructured Exception Handlingの略）がないとバイナリーを実行できません。動作確認する前に`${CMAKE_INSTALL_PREFIX}/bin/`以下にこれらのDLLをコピーします。

```text
% cd /tmp/local.windows/bin
% cp $(x86_64-w64-mingw32-g++ -print-file-name=libstdc++-6.dll) ./
% cp $(x86_64-w64-mingw32-g++ -print-file-name=libgcc_s_seh-1.dll) ./
```


これで動作確認できます。

```text
% wine groonga.exe --version
Groonga 6.1.1 [Windows,x64,utf8,match-escalation-threshold=0,nfkc,onigmo]

configure options: <>
```


[CPack](https://cmake.org/cmake/help/v3.7/module/CPack.html)を使えばビルドしたバイナリーを含むアーカイブを作成することができます。[PGroonga](https://pgroonga.github.io/ja/)は[CPackを使って](https://github.com/pgroonga/pgroonga/blob/1.1.9/CMakeLists.txt#L110-L124)バイナリー入りアーカイブを作成しています。

`CMakeLists.txt`にCPackの設定がある場合は次のようにすればアーカイブを作成できます。

```text
% make package
```


### まとめ

Groongaを例にしてDebian GNU/Linux上でCMakeを使ってWindows用のバイナリーをビルドする方法を紹介しました。CMakeを使っているプロダクトをクロスコンパイルしたいときは参考にしてください。
