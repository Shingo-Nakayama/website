---
tags: []
title: セキュリティの神話
---
先月、オライリージャパンからセキュリティの神話という本が出版されました。McAfeeの人が書いたセキュリティに関するエッセイ集の翻訳です。
<!--more-->


[https://amazon.co.jp/dp/4873114519](https://amazon.co.jp/dp/4873114519)

セキュリティ業界の人が建前無しでどんなことを考えているか、を気軽に読みたいならよい本でしょう。この本を読んでセキュリティの考えを深めたいというよりも、セキュリティについて視点を広げたいという方に向いているでしょう。

### 概要

それぞれの章の内容はほぼ独立していて、アンチウィルスのこと以外の話題も多くあります。それぞれの章は、最初に話題の概要を話して、それに関する著者が思いつく考えをいくつも話して、最後に簡単にまとめる、という流れになっています。真ん中の著者の考えのところでは、1つの話題に対して何パターンも考えを書いています。おそらく、自分はそうは考えなかったという新しい視点が見つかるでしょう。

ただ、その考えがすべてがすべて素晴らしいものと感じるものではないでしょう（当然ですが）。むしろ、多くの考えはうーんと感じるかもしれません。

たびたび「McAfeeいいよ」みたいなことが書かれていますが、その一方で「ちゃんとセキュリティ対策ソフトを使え。俺は使わないけどね。」みたいなことが書かれています。「自分のところで作っているものを自分は使わないのに、いいよって言われても。。。」と感じてしまうところですが、おそらく、著者がホントに思っていることを書いたのが本書なんでしょう。そのため、セキュリティ業界にいる人の率直な考えを読むことができるといえます（よくも悪くも）。

### 内容例

著者が使っているほぼ100%検出する迷惑メール対策の方法も書かれています。個人でやるにはそれでもいいかもね、というような内容です。概要が書かれているだけですが、それを読む感じだといくつか問題がありそうです。

オープンソースソフトウェアとセキュリティについても書かれています。[エリック・レイモンド](https://ja.wikipedia.org/wiki/%E3%82%A8%E3%83%AA%E3%83%83%E3%82%AF%E3%83%BB%E3%83%AC%E3%82%A4%E3%83%A2%E3%83%B3%E3%83%89)の[伽藍とバザール](https://amazon.co.jp/dp/4895421686)を参照して、「オープンソースの方がクローズドなソフトウェアよりもセキュアと言われているけど、そうじゃないよね」、という話題です。ただ、脚注にも以下のように伽藍とバザールの内容が引用されている通り、エリック・レイモンドは「オープンソース＝セキュア」とは言っていないですよね。

> ベータテスタと共同開発者の基盤さえ十分大きければ、ほとんどすべての問題はすぐに見つけだされて、その直し方もだれかにはすぐわかるはず。
<p class="source">
  <cite><a href="http://cruel.org/freeware/cathedral.html">%E4%BC%BD%E8%97%8D%E3%81%A8%E3%83%90%E3%82%B6%E3%83%BC%E3%83%AB</a></cite>
</p>

オープンソースにしたからといってバザール方式でうまくまわるとは限らないということは、実際にやってみるとすぐにわかります。

ここでは、最終的に「オープンかクローズドかは関係ないよね」となるのですが、そこに至るまでに7個の考えを出しています。このようにいろいろなパターンの考えを読めるのは魅力でしょう（考えの内容が納得できるかどうかは別として）。

他にも「Googleは邪悪？」では、「Googleのやり方はよくない」みたいなことを書いています。同じようなことはMcAfeeには言えないだろうか、などと考えながら読むのもよいかもしれません。

### まとめ

セキュリティの神話という本の内容を紹介しました。気軽に読めるセキュリティ関係の読み物として読んでみてはいかがでしょうか。読みやすい日本語になっているところと、話題についての日本での状況が脚注として追加されているところは、原書にはない翻訳版ならではの嬉しいところでしょう。

Web上にいくつか言及しているページがあったので、リンクしておきます。

  * [セキュリティの神話 -- 研究とビジネスのあいだ - 生駒日記](http://d.hatena.ne.jp/mamoruk/20100425/p1)
  * [KeN's GNU/Linux Diary | 『セキュリティの神話』](http://kmuto.jp/d/index.cgi/review/myths-of-security.htm)
  * [セキュリティホール memo](http://www.st.ryukoku.ac.jp/~kjm/security/memo/2010/05.html#20100510__Python): "セキュリティの神話も好評 (?) 発売中。セキュリティぶっちゃけ話、という感じの楽しい本。"
  * [デ　　ザ　　イ　　ン　　思　　考 / d e s i g n - t h i n k i n g: 私のダークな仕事について](http://www.design-thinking.jp/2010/05/blog-post_12.html)
  * [デ　　ザ　　イ　　ン　　思　　考 / d e s i g n - t h i n k i n g: 私のダークな仕事について（つづき）〜セキュリティの神話](http://www.design-thinking.jp/2010/05/blog-post_13.html)

いくつか誤植があるので、読んでがっかりする前に、まず正誤表を見てこのくらいあるんだなと思っておいた方がよいでしょう。読んでいるうちにいくつか見つけものがありましたが、それは報告してあるので、しばらくすれば正誤表に反映されたり、増刷の時に書籍に反映されたりするでしょう。

  * [O'Reilly Japan - セキュリティの神話 - 第1刷正誤表](http://www.oreilly.co.jp/books/9784873114514/#errata0)

[https://amazon.co.jp/dp/4873114519](https://amazon.co.jp/dp/4873114519)

最近のセキュリティまわりの話題にも興味がある場合はセキュリティExpert 2010も読んでみるとよいでしょう。

[https://amazon.co.jp/dp/4774142174](https://amazon.co.jp/dp/4774142174)
