---
title: fluent-package v5への更新【Fluentd.org記事翻訳】
author: yoshimotoy
tags:
- fluentd
---

今回の記事は、2023年7月に英語でリリースされた[Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5) [^1]の翻訳記事になります。

[^1]: Written with Apache Licence 2.0 http://www.apache.org/licenses/LICENSE-2.0. Copy rights 2023 Fluentd project.  内容を日本語に翻訳。

<div class="callout secondary">
<ul>
<li>2023年8月にリリースされたFluent Package及びFluent Package LTS(Long Term Support)の説明</li>
<li>更新の手順</li>
</ul>
が含まれています
</div>

<!--more-->


### 前段

fluent-package"v5"は、2023年8月から利用可能になっています。fluent-packageは td-agent"v4"の後継として作られました。
この投稿では、開発段階でテストした手順を共有します。これがv4からv5への更新の実践に役立つことを願っています。

## パッケージ名称の変更理由
`fluent-package`は、以前`td-agent`として知られていたものです。昔は、[Treasure Data社](https://www.treasuredata.com/)が、中心となってパッケージの提供を行っていました。現在は、Fluentdのコミュニティでパッケージ提供を行っています。この開発体制の変更が今回のパッケージ名の変更の理由です。

「Fluentdと関連するgemパッケージを含むFluentdのオールインワンパッケージ」を表現するために、パッケージ名が`fluent-package`に変更されました。

パッケージの名称は変更されましたが、現在でもTreasure Data社が、引き続きパッケージの配信リソースのスポンサーをしています。

## どのチャンネルを使うべきか

`fluent-package`には、2種類のチャンネルがあります。
 * Normal release version = 通常リリース版
 * Long Term Support version (LTS) = 長期サポート版

一つは、定期的に更新される通常リリースバージョンです。つまり、ラピッドリリース開発スタイルで提供されます。 (td-agent v4 は、ほぼ四半期ごとにこのようにリリースされました)。このバージョンでは、同梱されているfluentdは最終的に新しいマイナーバージョン (例: 1.17.xなど)に更新されます。（そのため、新しい機能が含まれることになります。）

もう一つは、新しい機能が導入されない、より保守的なメンテナンスの行われるバージョン(長期サポート)です。セキュリティ修正やバグ修正などの小さなアップデートのみが適用されます。 v5のLTSは2025年3月までサポートされる予定です。

通常リリース版とLTS版の違いについては、[「Fluent Packageに関するサポートライフサイクル予定のお知らせ(※英語)」](https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle)で詳しく説明しています。


## td-agent v4とfluent-package v5の違い

Fluent-package v5では、ruby (2.7.8 -> 3.2.2) や OpenSSL (1.1.1 -> 3.1.0 (Windows の場合)、3.0.8 (macOS の場合)) などのコアコンポーネントが更新されました。

主な変更点は以下の通りです。

  * コマンド名`td-agent`が`fluentd`に変更
    * `$ td-agent --version` -> `$ fluentd --version`<!--ここはfluentではない？-->
  * コマンド名`td-agent-gem`が`fluent-gem`に変更
    * `$ td-agent-gem list` -> `$ fluent-gem list`
  * Windows以外のサービス名称`td-agent`が`fluentd`に変更
    * `$ systemctl status td-agent` -> `$ systemctl status fluentd`

パッケージ名の変更に伴い、インストールパス、サービス名（/opt/fluent、fluentd.serviceなど）等も変更されました。基本的に、td-agent v4ユーザーに対しては、古いファイルをコピーしたり、シンボリックリンクを提供したりして移行プロセスを実行することで、可能な限り互換性を維持することを目指しました。

アップグレードされたコンポーネントの詳細を知りたい場合は、[CHANGELOG.md](https://github.com/fluent/fluent-package-builder/blob/master/CHANGELOG.md#release-v500---20230729)を参照してください。

注: プラットフォーム固有の問題については、「v4 ユーザー向けの追加ヒント」セクションで説明します。

## アップグレード手順

td-agentにバンドルされているプラ​​グインは、fluent packageにアップグレートを行う際に、自動的にアップグレードされます。ただし、自分で追加した他のプラグインは自動的にアップグレードされません。 v4およびv5のディレクトリ構造の一部が変更されているため、プラグインを別にアップグレードする必要があるかどうかを確認する必要があります。

ここでは、「fluent-plugin-concat」などのプラグインを独自に追加した手順を示します。以下は、手順で使用したサンプル構成ファイルです。

```
<filter docker.log>
  @type concat
  key message
  multiline_start_regexp /^Start/
</filter>
```

### 1. td-agentにどのプラグインが一緒にインストールされているかをレビューする

```
$ td-agent-gem list | grep fluent-plugin*
fluent-plugin-calyptia-monitoring (0.1.3)
fluent-plugin-concat (2.5.0)
fluent-plugin-elasticsearch (5.3.0)
fluent-plugin-flowcounter-simple (0.1.0)
fluent-plugin-kafka (0.19.0)
fluent-plugin-metrics-cmetrics (0.1.2)
fluent-plugin-opensearch (1.1.0)
fluent-plugin-prometheus (2.0.3)
fluent-plugin-prometheus_pushgateway (0.1.0)
fluent-plugin-record-modifier (2.1.1)
fluent-plugin-rewrite-tag-filter (2.4.0)
fluent-plugin-s3 (1.7.2)
fluent-plugin-sd-dns (0.1.0)
fluent-plugin-systemd (1.0.5)
fluent-plugin-td (1.2.0)
fluent-plugin-utmpx (0.5.0)
fluent-plugin-webhdfs (1.5.0)
```

`/opt/td-agent/lib/ruby/gems/2.7.0/gems/ `ディレクトリー以下でもインストールされているプラグインを見つけることができます。

```
$ ls -l /opt/td-agent/lib/ruby/gems/2.7.0/gems |grep fluent-plugin*
drwxr-xr-x. 5 root root  175  7月 14 03:01 fluent-plugin-calyptia-monitoring-0.1.3
drwxr-xr-x. 4 root root  206  7月 14 03:03 fluent-plugin-concat-2.5.0
drwxr-xr-x. 5 root root 4096  7月 14 03:01 fluent-plugin-elasticsearch-5.3.0
drwxr-xr-x. 4 root root  205  7月 14 03:01 fluent-plugin-flowcounter-simple-0.1.0
drwxr-xr-x. 6 root root  191  7月 14 03:01 fluent-plugin-kafka-0.19.0
drwxr-xr-x. 5 root root  190  7月 14 03:01 fluent-plugin-metrics-cmetrics-0.1.2
drwxr-xr-x. 5 root root 4096  7月 14 03:01 fluent-plugin-opensearch-1.1.0
drwxr-xr-x. 5 root root  215  7月 14 03:01 fluent-plugin-prometheus-2.0.3
drwxr-xr-x. 6 root root  238  7月 14 03:01 fluent-plugin-prometheus_pushgateway-0.1.0
drwxr-xr-x. 4 root root  176  7月 14 03:01 fluent-plugin-record-modifier-2.1.1
drwxr-xr-x. 3 root root  210  7月 14 03:01 fluent-plugin-rewrite-tag-filter-2.4.0
drwxr-xr-x. 5 root root  230  7月 14 03:01 fluent-plugin-s3-1.7.2
drwxr-xr-x. 3 root root  170  7月 14 03:01 fluent-plugin-sd-dns-0.1.0
drwxr-xr-x. 3 root root   49  7月 14 03:01 fluent-plugin-systemd-1.0.5
drwxr-xr-x. 5 root root  221  7月 14 03:01 fluent-plugin-td-1.2.0
drwxr-xr-x. 5 root root  166  7月 14 03:01 fluent-plugin-utmpx-0.5.0
drwxr-xr-x. 4 root root  191  7月 14 03:01 fluent-plugin-webhdfs-1.5.0
```

### 2. td-agent v4のデーモンを止める

```
$ sudo systemctl stop td-agent
```

`fluent-package`は、サービスを止めないままの更新をサポートしていますが、ここでは、明確に止めることをおすすめします。

### 3. fluent-package v5のインストールスクリプトを実行する

RedHat 及び派生のディストリビューションを使っている場合で、fluent-package通常版をインストールしたい場合は以下のスクリプトを実行してください。

```
# curl -L https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5.sh | sh
```

RedHat 及び派生のディストリビューションを使っている場合で、fluent-packageLTS版をインストールしたいなら以下のスクリプトを実行してください。

```
# curl -L https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5-lts.sh | sh
```

もっと細かいインストールについての情報は公式ドキュメント[Fluend Doc - Installation](https://docs.fluentd.org/installation)を参照してください。

### 4. fluent-package v5がきちんとインストールされていることの確認

```
$ LANG=C yum info fluent-package
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: ftp.riken.jp
 * extras: ftp.riken.jp
 * updates: ftp.riken.jp
Installed Packages
Name        : fluent-package
Arch        : x86_64
Version     : 5.0.0
Release     : 1.el7
Size        : 64 M
Repo        : installed
From repo   : /fluent-package-5.0.0-1.el7.x86_64
Summary     : The stable distribution of Fluentd
URL         : https://www.treasuredata.com/
License     : ASL 2.0
Description : The stable distribution of Fluentd, called td-agent.
```

### 5. fluent-packageのデーモンを再読み込み

```
$ sudo systemctl daemon-reload
$ sudo systemctl enable --now fluentd
```

### 6. インストールされているプラグインの確認

```
$ fluent-gem list |grep fluent-plugin*
fluent-plugin-calyptia-monitoring (0.1.3)
fluent-plugin-elasticsearch (5.3.0)
fluent-plugin-flowcounter-simple (0.1.0)
fluent-plugin-kafka (0.19.0)
fluent-plugin-metrics-cmetrics (0.1.2)
fluent-plugin-opensearch (1.1.0)
fluent-plugin-prometheus (2.0.3)
fluent-plugin-prometheus_pushgateway (0.1.0)
fluent-plugin-record-modifier (2.1.1)
fluent-plugin-rewrite-tag-filter (2.4.0)
fluent-plugin-s3 (1.7.2)
fluent-plugin-sd-dns (0.1.0)
fluent-plugin-systemd (1.0.5)
fluent-plugin-td (1.2.0)
fluent-plugin-utmpx (0.5.0)
fluent-plugin-webhdfs (1.5.0)
```

バンドルされているプラ​​グインもアップグレードされていることがわかりますが、独自に追加されたプラグインは見つかりません。今回の例では、"fluent-plugin-concat" が追加されていますが、インストールされているプラグインリストには表示されていません。

### 7. 個別に追加したプラグインをインストールする

```
$ sudo fluent-gem install fluent-plugin-concat
```

```
$ fluent-gem list | grep fluent-plugin*
fluent-plugin-calyptia-monitoring (0.1.3)
fluent-plugin-concat (2.5.0)
fluent-plugin-elasticsearch (5.3.0)
fluent-plugin-flowcounter-simple (0.1.0)
fluent-plugin-kafka (0.19.0)
fluent-plugin-metrics-cmetrics (0.1.2)
fluent-plugin-opensearch (1.1.0)
fluent-plugin-prometheus (2.0.3)
fluent-plugin-prometheus_pushgateway (0.1.0)
fluent-plugin-record-modifier (2.1.1)
fluent-plugin-rewrite-tag-filter (2.4.0)
fluent-plugin-s3 (1.7.2)
fluent-plugin-sd-dns (0.1.0)
fluent-plugin-systemd (1.0.5)
fluent-plugin-td (1.2.0)
fluent-plugin-utmpx (0.5.0)
fluent-plugin-webhdfs (1.5.0)
```

fluent-package v5のために、"/opt/fluent/lib/ruby/gems/3.2.0/gems/" ディレクトリー下に"fluent-plugin-concat" がインストールされました。

``` 
$ ls -l /opt/fluent/lib/ruby/gems/3.2.0/gems/ |grep fluent-plugin*
drwxr-xr-x.  5 root root  175  7月 14 03:14 fluent-plugin-calyptia-monitoring-0.1.3
drwxr-xr-x.  4 root root  206  7月 14 03:16 fluent-plugin-concat-2.5.0
drwxr-xr-x.  5 root root 4096  7月 14 03:14 fluent-plugin-elasticsearch-5.3.0
drwxr-xr-x.  4 root root  205  7月 14 03:14 fluent-plugin-flowcounter-simple-0.1.0
drwxr-xr-x.  6 root root  191  7月 14 03:14 fluent-plugin-kafka-0.19.0
drwxr-xr-x.  5 root root  190  7月 14 03:14 fluent-plugin-metrics-cmetrics-0.1.2
drwxr-xr-x.  5 root root 4096  7月 14 03:14 fluent-plugin-opensearch-1.1.0
drwxr-xr-x.  5 root root  215  7月 14 03:14 fluent-plugin-prometheus-2.0.3
drwxr-xr-x.  6 root root  238  7月 14 03:14 fluent-plugin-prometheus_pushgateway-0.1.0
drwxr-xr-x.  4 root root  176  7月 14 03:14 fluent-plugin-record-modifier-2.1.1
drwxr-xr-x.  3 root root  210  7月 14 03:14 fluent-plugin-rewrite-tag-filter-2.4.0
drwxr-xr-x.  5 root root  230  7月 14 03:14 fluent-plugin-s3-1.7.2
drwxr-xr-x.  3 root root  170  7月 14 03:14 fluent-plugin-sd-dns-0.1.0
drwxr-xr-x.  3 root root   49  7月 14 03:14 fluent-plugin-systemd-1.0.5
drwxr-xr-x.  5 root root  221  7月 14 03:14 fluent-plugin-td-1.2.0
drwxr-xr-x.  5 root root  166  7月 14 03:14 fluent-plugin-utmpx-0.5.0
drwxr-xr-x.  4 root root  191  7月 14 03:14 fluent-plugin-webhdfs-1.5.0
```

### 8. fluent-package v5のデーモンを開始する

```
$ sudo systemctl start fluentd
```

### 9. fluentdのログにエラーメッセージがないことを確認する

```
$ tail -100f /var/log/fluent/fluentd.log
```
これで、更新手順は完了です。ログ収集を楽しんでください！

## v4ユーザーへの追加ヒント

### For Debian/Ubuntu

* `fluentd-apt-source`パッケージは、移行用のパッケージとしてマークされます。`sudo apt purge fluentd-apt-source`を実行することで、安全に削除することができます。
* `td-agent.service`を有効にしたい場合は、明示的に以下のコマンドを実行しなくてはなりません。

```
$ sudo systemctl unmask td-agent
$ sudo systemctl enable fluentd
```

### For RHEL

* `td-agent.service`を有効にしたい場合は、明示的に以下のコマンドを実行しなくてはなりません。

```
$ sudo systemctl enable fluentd
```

### For Windows

`fluentd-package`インストーラーはデフォルトでサービスを起動しなくなりました。`fluentd`をサービスとして開始したい場合はアドミニストレーター権限で明示的に下記のコマンドを実行してください。

```
c:\opt\fluent> net start fluentdwinsvc
```

### For macOS

現在のところ、fluent-packageのdmgバージョンを正式にサポートする予定はまだありません。最小限の構築が可能な状態になるように変更されているだけであり、テスト目的のみでしか使えません。


# まとめ

クリアコードがコミュニティにも関わっているFluentdの新しいパッケージについての本家の英語記事を翻訳しました。必要としている方にとって便利だと思ってもらえると嬉しいです。移行などで問題があった場合は[サポートサービス]({% link services/fluentd-service.md %})にてサポート可能ですので、お気軽にお問合せください。

新パッケージが出た際に[「リリース自慢会」](https://youtube.com/live/CrsTNHqWHTc)を開催したのですが、その中でも移行に関する部分の報切り抜いた動画もあるので、あわせてごらんください。
<div class="youtube">
<iframe width="560" height="315" src="https://www.youtube.com/embed/e5Lfnze092A?si=23tehPhvOuq6RM0u" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
