---
tags: []
title: reportbugを使ってバグ報告をしてみよう
---
[Software Design 2015年3月号](http://gihyo.jp/magazine/SD/archive/2015/201503)の「Debian Hot Topics」でreportbugを使ってバグ報告をする方法についての記事がありました。
reportbugを使ったことがなかったので、最近踏んだバグについてreportbugを使ってバグ報告をしたときのことを紹介します。
<!--more-->


### 遭遇したバグについて

[DCMTK](http://dicom.offis.de/dcmtk.php.en)というDICOMのToolkitをインストールしたときに、Apacheの設定をa2enconfで有効にできないことに気づきました。

{% raw %}
```
% sudo a2enconf dcmtk
ERROR: Conf dcmtk does not exist!
```
{% endraw %}

丁度reportbugの記事を読んだのでreportbugを使ってバグ報告することにしました。

### reportbugでバグ報告するには

reportbugでバグ報告するには以下の手順を踏みます。

  * reportbugの初期設定をする
  * reportbugを起動する
  * 対象となるパッケージを選択する
  * 新しいバージョンを確認する
  * 既知のバグを確認する
  * 変更したファイルを添付する
  * バグに適切なタイトルをつける
  * バグの重要度を決める
  * バグの詳細を記述する
  * バグを送信する

それぞれ、順に説明します。

#### reportbugの初期設定をする

まずは、初期設定が必要です。
reportbugコマンドを実行すると対話形式で設定ができます。

{% raw %}
```
% reportbug
Please choose the default operating mode for reportbug.

1 novice    Offer simple prompts, bypassing technical questions.

2 standard  Offer more extensive prompts, including asking about things that a moderately sophisticated user would be expected to know about Debian.

3 advanced  Like standard, but assumes you know a bit more about Debian, including "incoming".

4 expert    Bypass most handholding measures and preliminary triage routines. This mode should not be used by people unfamiliar with Debian's policies and operating procedures.

Select mode: [novice] 
Please choose the default interface for reportbug.

1 text  A text-oriented console user interface

2 gtk2  A graphical (GTK+) user interface.

Select interface: 2
Will reportbug often have direct Internet access? (You should answer yes to this question unless you know what you are doing and plan to check whether duplicate reports have been filed via some other channel.) [Y|n|q|?]? Y
What real name should be used for sending bug reports?
[HAYASHI Kentaro]> 
Which of your email addresses should be used when sending bug reports? (Note that this address will be visible in the bug tracking system, so you may want to use a webmail address or another address with good spam filtering capabilities.)
[hayashi@clear-code.com]> 
Do you have a "mail transport agent" (MTA) like Exim, Postfix or SSMTP configured on this computer to send mail to the Internet? [y|N|q|?]? 
Please enter the name of your SMTP host. Usually it's called something like "mail.example.org" or "smtp.example.org". If you need to use a different port than default, use the <host>:<port> alternative format. Just press ENTER if you don't have one or don't know, and
so a Debian SMTP host will be used.
> (SMTPサーバーを指定)
If you need to use a user name to send email via "mail.clear-code.com" on your computer, please enter that user name. Just press ENTER if you don't need a user name.
> (メールアカウントを指定)
Do you want to encrypt the SMTP connection with TLS (only available if the SMTP host supports it)? [y|N|q|?]? y
Please enter the name of your proxy server. It should only use this parameter if you are behind a firewall. The PROXY argument should be formatted as a valid HTTP URL, including (if necessary) a port number; for example, http://192.168.1.1:3128/. Just press ENTER if
you don't have one or don't know.
> 
Default preferences file written. To reconfigure, re-run reportbug with the "--configure" option.
```
{% endraw %}

上記のように設定をおこなったときの設定ファイルの保存先は~/.reportbugrcです。パスワードは設定されないので、適宜smtppasswdのエントリを編集します。

#### reportbugを起動する

初期設定ができたら、reportbugを起動します。初期設定でGTK2のインターフェースを選択したので、GUIで起動できます。

![reportbug起動時の画面]({{ "/images/blog/20150304_0.png" | relative_url }} "reportbug起動時の画面")

#### 対象となるパッケージを選択する

次にバグ報告の対象となるパッケージを選択します。

![バグ報告対象のパッケージ指定画面]({{ "/images/blog/20150304_1.png" | relative_url }} "バグ報告対象のパッケージ指定画面")

#### 新しいバージョンを確認する

新しいバージョンを自動的に確認します。もし更新されたパッケージがあるならそちらで再現するか確認するとよいでしょう。

![自動的に更新されたパッケージの確認画面]({{ "/images/blog/20150304_2.png" | relative_url }} "自動的に更新されたパッケージの確認画面")

#### 既知のバグを確認する

バグ報告する前に既知のバグに該当するものがないか確認します。

![既知のバグリスト画面]({{ "/images/blog/20150304_3.png" | relative_url }} "既知のバグリスト画面")

#### 変更したファイルを添付する

もし変更した設定ファイルがあると自動的に検出してくれます。設定ファイルは問題の切り分けに有効です。添付しておきましょう。

![変更したファイルの添付画面]({{ "/images/blog/20150304_4.png" | relative_url }} "変更したファイルの添付画面")

#### バグに適切なタイトルをつける

バグの内容をわかりやすく説明したタイトルをつけます。
dcmtkの設定ファイルをa2enconfが認識できないので「/etc/dcmtk/apache.conf is not recognized by a2enconf」にしました。

![バグのタイトル指定画面]({{ "/images/blog/20150304_5.png" | relative_url }} "バグのタイトル指定画面")

#### バグの重要度を決める

バグがどの程度深刻なものかを指定します。
設定ファイルが認識されないので、「important」にしました。

![バグの重要度の指定画面]({{ "/images/blog/20150304_6.png" | relative_url }} "バグの重要度の指定画面")

#### バグの詳細を記述する

次にバグの詳細を記述します。
あらかじめ雛形が表示されるので、適宜編集します。

![バグ報告の雛形画面]({{ "/images/blog/20150304_7.png" | relative_url }} "バグ報告の雛形画面")

求められているのは、以下の4つです。

  * 状況の説明
  * 何をしたら効果があったのか or 効果がなかったのか
  * 作業の結果どうなったか
  * 期待している結果

雛形にそって詳細を記述しました。

![バグ報告の詳細記述画面]({{ "/images/blog/20150304_8.png" | relative_url }} "バグ報告の詳細記述画面")

#### バグを送信する

バグの記述を終えたらあとは送信するだけです。

このまま送信してもいいですし、一旦ファイルへと保存しておいて、あとで報告することもできます。

![バグ報告方法指定画面]({{ "/images/blog/20150304_9.png" | relative_url }} "バグ報告方法指定画面")

他に追記することもなかったので、そのまま送信しました。

![バグ報告完了画面]({{ "/images/blog/20150304_10.png" | relative_url }} "バグ報告完了画面")

これで、[dcmtk-www: /etc/dcmtk/apache.conf is not recognized by a2enconf](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=779700)としてバグ報告が登録されました。

### まとめ

フリーソフトウェアを使っているときに、バグを踏んでしまった経験はありませんか？そのとき、開発元にバグ報告をしていますか？あるいは回避策をとってそのままにしていたりしませんか？
踏んだバグをそのままにしてしまうと、他の人も同じバグを踏むことになります。パッチを作成するところまでは無理でも、適切なバグ報告があると開発者の修正の助けになります。

今回は、バグ報告のための専用ツールであるreportbugを実際に使ってバグ報告をしたときのことを紹介しました。
[Software Design 2015年3月号](http://gihyo.jp/magazine/SD/archive/2015/201503)の記事では、CUIで報告する例でしたが、初期設定によってはGUIでバグ報告することもできます。機会があったら使ってみてください。
