---
title: "Fluentd: in_tailプラグインの基本的な使い方をメンテナーが解説"
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

Fluentdは、様々なデータソースからデータを収集し、様々な出力先へ転送することができる便利なフリーソフトウェアです！

Fluentdでは、プラグインを組み合わせることで様々な用途を実現できます。
その中でも、ファイルからログを収集したい場合には[in_tailプラグイン](https://docs.fluentd.org/input/tail)がよく使われます。

しかし、ファイルと一言で言っても、その更新のされ方やログローテートのされ方は様々です。
加えて、`in_tail`プラグインにはとても多くの設定項目があります。

本記事では、

* まずはこれを把握すれば、大体のケースで`in_tail`プラグインを問題なく使える！

という点をFluentdメンテナー視点でいくつか解説します。

Fluentdを使ってみたいけど設定がよく分からず使えていない、とか、使っているけど設定にあまり自信がない、という方は、ぜひご覧ください。

<!--more-->

(以下の説明は Fluentd v1.16.3 時点のものになります)

## 最初に注意！

Fluentd v1.16.3 (fluent-package v5.0.2, td-agent v4.5.2)以降のバージョンを使用してください。

`in_tail`プラグインでは、エラーを出さずに収集が停止してしまう、といった大きな不具合が過去にありました。
Fluentd v1.16.3 (fluent-package v5.0.2, td-agent v4.5.2)時点でそのような大きな不具合を修正しています。

詳しくは [Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正]({% post_url 2023-12-15-fluent-package-v5.0.2 %}) の記事で紹介しているので、ご覧ください。

## 動作確認の仕方

Fluentdは簡単に手元で動作を確認することができます。
Fluentdの簡単な動かし方については、以前の記事 [Fluentdを動かしてみよう！]({% post_url 2024-02-16-fluentd-tutorial-run-fluentd %}) で解説しているので、ぜひご覧ください。

ぜひ今回も実際に`in_tail`の動きを確認してみてください。

[Fluentdを動かしてみよう！]({% post_url 2024-02-16-fluentd-tutorial-run-fluentd %}) では、次の設定でFluentdが実際にデータを処理する様子を確認しました。

```xml
<source>
  @type sample
  tag test
</source>

<match test.**>
  @type stdout
</match>
```

この`source`設定の部分を、[in_sampleプラグイン](https://docs.fluentd.org/input/sample)から[in_tailプラグイン](https://docs.fluentd.org/input/tail)の設定に置き換えてください。

```xml
<source>
  @type tail
  tag test
  path /tmp/test.log
  pos_file /tmp/tail-test.pos
  <parse>
    @type none
  </parse>
</source>

<match test.**>
  @type stdout
</match>
```

そうすれば、 [Fluentdを動かしてみよう！]({% post_url 2024-02-16-fluentd-tutorial-run-fluentd %}) と同じように、Fluentdが出力するログメッセージとして`in_tail`の収集ログを確認することができます。

## 最も基本的な使い方

まずは最も基本的な使い方を説明します。

### 設定

`in_tail`は[Inputプラグイン](https://docs.fluentd.org/input)なので、[sourceセクション](https://docs.fluentd.org/configuration/config-file#id-1.-source-where-all-the-data-comes-from)を足して次の5つの設定をします。

* `@type`
  * 使いたいプラグインの名前を指定します。名前はそのプラグインのドキュメントを確認しましょう。[今回は"tail"です](https://docs.fluentd.org/input/tail#example-configuration)[^type-naming]。
* `tag`
  * 後続の[Filterプラグイン](https://docs.fluentd.org/filter)や[Outputプラグイン](https://docs.fluentd.org/output)へのルーティング等に使うタグを指定します。今回はテスト用に`test`とします。
  * 詳しく知りたい方は [Config File Syntax - Interlude: Routing](https://docs.fluentd.org/configuration/config-file#interlude-routing) や [How do the match patterns work?](https://docs.fluentd.org/configuration/config-file#how-do-the-match-patterns-work) をご覧ください。
* [path](https://docs.fluentd.org/input/tail#path)
  * 収集したいファイルのファイルパスを指定します。
* [pos_file](https://docs.fluentd.org/input/tail#pos_file-highly-recommended)
  * どこまで収集したかの記録を残すためのファイルパスを指定します。
  * `in_tail`設定を複数行う場合は、それぞれ別々のパスを指定してください。
* [parseセクション](https://docs.fluentd.org/input/tail#less-than-parse-greater-than-directive-required)
  * 収集したログのパース設定を行います。好きな[Parserプラグイン](https://docs.fluentd.org/parser)を使えます。そのまま出力したい場合は、[parser_noneプラグイン](https://docs.fluentd.org/parser/none)を設定すればよいです（つまり`@type none`を指定します)。

以上を設定すると、例えば次のような`source`設定になります。

```xml
<source>
  @type tail
  tag test
  path /tmp/test.log
  pos_file /tmp/tail-test.pos
  <parse>
    @type none
  </parse>
</source>
```

### `in_tail`の動作

この設定を動かしてみると、`in_tail`は`/tmp/test.log`ファイルの末尾を監視して、末尾に追記されるログをどんどん収集します。
一度Fluentdを停止しても、`pos_file`にどこまで収集したかの記録が残るため、次回起動時には前回の続きから漏れなく収集してくれます。

前の章の「動作確認の仕方」で説明しているように、[out_stdoutプラグイン](https://docs.fluentd.org/output/stdout)と組み合わせて動作を確認できます。
例えば次のようにファイルにログを追記すると、

```bash
echo "test log" >> /tmp/test.log
```

次のようなログをFluentdが出力するので、`in_tail`がログを収集したことが分かります。

```
2024-03-13 14:49:02.480691808 +0900 test: {"message":"test log"}
```

補足: `in_tail`がファイルを発見して監視している状態であれば、新規に追記したログをすばやく（基本的に数秒以内に）収集します。
ただし、もし新しくファイルを作成した場合、[refresh_interval](https://docs.fluentd.org/input/tail#refresh_interval)（デフォルト60秒）の間隔で監視ファイルリストを更新するため、1分程度待つ必要があります。

### 補足: 権限について

`path`や`pos_file`に指定するファイルパスにFluentdがアクセスできる十分な権限がないと、うまく動かないので注意が必要です。
権限が足りない場合、`Permission denied @ rb_sysopen - /tmp/test.log`のようなエラーが出ます。

* 例: `path`に対する読み込み権限が足りない
  ```
  2024-03-14 11:53:42 +0900 [warn]: #0 Permission denied @ rb_sysopen - /tmp/test.log
  ```
* 例: `pos_file`に対する書き込み権限が足りない
  ```
  2024-03-14 11:45:24 +0900 [error]: #0 unexpected error error_class=Errno::EACCES error="Permission denied @ rb_sysopen - /tmp/tail-test.pos"
  ```

例えば、Unix系OSにおいてこのように権限が足りない場合は、`chmod`や`chown`コマンドを使って権限を調整したり、ユーザーやグループ設定を調整したりします（パッケージ「Fluent Package」は、デフォルトで`fluentd`ユーザーおよびグループでサービスを実行します）。

`pos_file`は読み込みと書き込みの双方の権限が必要です。
パッケージ「Fluent Package」の場合、デフォルトで`/var/log/fluentd/`が書き込み可能なパスとして作成されているので、`pos_file`に`/var/log/fluentd/pos/tail-test.pos`のようなパスを指定するのもよいでしょう。

`path`は読み込み権限が必要です。
例えば、syslogが出力する一部のファイルには読み込み権限がないことがあります。
この場合、単に`chmod`や`chown`コマンドを使ってカレントファイルの権限を調整するだけでなく、今後ログローテート時に新規作成されるファイルの権限も調整する必要があります。
本記事では詳しい説明を省きますが、必要に応じてsyslog側の設定の調整も検討してください。

### 前半まとめ

ここまでで基本的な`in_tail`の設定と動きを確認しました。

以下では、`in_tail`を使う際に考慮する必要がある主な点として、以下の4点をそれぞれ説明します。

* `read_from_head`: （初回起動時に）新規ファイルを先頭から読ませる
* ログローテートへの対応
* カレントファイルのファイル名が変化する出力形式への対応
* `in_tail`で収集するべきファイルであるかどうかの確認

まずはこれらの点を把握すれば、大体のケースで`in_tail`を問題なく使えるはずです！
より高度な使い方をしたい場合は、[公式ドキュメント](https://docs.fluentd.org/input/tail)でどんな設定があるのかを確認してみてください。
本記事では紹介しきれない様々な機能を`in_tail`は持っています！

## `read_from_head`: （初回起動時に）新規ファイルを先頭から読ませる

`in_tail`は、ファイル末尾への追記を収集し続ける性質のため、ファイル内容を全て読み込むとは限りません。

一度そのファイルの収集を開始した後は、`pos_file`の記録を元にそれ以降の追記を漏れなく収集します（一時的にFluentdが停止したとしても）。
しかし、最初にファイルの収集を開始するときには、デフォルトではファイルの末尾から開始するため、既存のファイル内容は収集されません。

そのため、基本的には初回起動時のみ気にするべき話になります。
もし初回起動時にファイルの既存の内容を収集してほしい場合は、[read_from_head](https://docs.fluentd.org/input/tail#read_from_head)を`true`に設定します。

### 過去に`read_from_head`設定が推奨されていた経緯

過去のバージョンで、`read_from_head`設定が一部の設定に実質必須であった、という経緯がありました。
そのため、とりあえず`read_from_head true`を設定しておいた方が安心、という話を今でも聞くことがあります。
しかし、最近のバージョンでは状況が異なります。

Fluentd v1.14.3 (td-agent v4.3.0) 以降のバージョンでは、このパラメターはFluentd起動時の動作にしか影響しません。
そのため、これ以降のバージョンでは、

* Fluentd起動時に新規ファイルの内容を全部収集したい

という場合のみ`read_from_head true`を設定する、という考え方で問題ありません。

補足: Fluentd v1.14.3 (td-agent v4.3.0) よりも前のバージョンでは、`path`に日付フォーマット（`%Y%m%d`など）を使う場合や[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)機能を使う場合には`read_from_head true`が実質必須でした。
それ以降のバージョンでは、こういった動的に新規ファイルを発見する機能においては`read_from_head`設定に依らずにファイルの内容を全て読み込むように修正されています。

## ログローテートへの対応

多くの場合、末尾への追記形式のログファイルはログローテートします。
`in_tail`はログローテートにうまく対応しますが、形式によっては注意が必要なケースもあります。

### `in_tail`がログローテートに対応する仕組み

`in_tail`は対象ファイルを監視していますが、一定の条件を満たすと対象ファイルにログローテートが発生したと検知します。
ログローテート発生を検出すると、対象ファイルの先頭から収集を再開します。
これによって、ログローテートが発生しても、漏れなく新しいログファイルの収集を継続します。

ログローテート検知の条件は、次の2つの少なくともどちらか一方を満たすことです。

* A: inode番号(ファイル固有のid)が変わる
* B: ファイルサイズが小さくなる

Aを満たすようなログローテート形式であれば、全く気にすることはありません。
安定してログローテートを検知できます。

一方で、Aを満たさないようなログローテート形式の場合は注意が必要です。
Bのファイルサイズでしか判定できないからです。

次の章で詳しく説明します。

### 対象ファイルのログローテート形式

前章の「A: inode番号(ファイル固有のid)が変わる」を満たすようなログローテート形式は、次のような動きになります。

1. カレントファイルをリネームする: `test.log` -> `test.log.1`
1. 新規カレントファイルを作成する: `test.log`を新規作成

Linux系のシステムでよく見られるのがこの形式です。

一方で、次のようなログローテート形式も存在します。

1. カレントファイルの内容をコピーする: `test.log`の内容を`test.log.1`にコピー
1. カレントファイルの内容をクリアする: `test.log`のサイズが0になる

結果としては最初の形式と同じに見えます。
しかし、カレントファイルの中身は初期化されていますが、ファイル自体は以前と同じファイルのままです。
そのため、ファイル固有のidであるinode番号が変化しておらず、前章の「A: inode番号(ファイル固有のid)が変わる」の条件を満たしません。

この他にも、世の中にはいくつかのログローテート形式が存在します。
`in_tail`を使う際には、inode番号が変わる（カレントファイルが新規生成される）ようなログローテート形式なのかどうかを確認してください。

もし、そうでない場合は、前章の「B: ファイルサイズが小さくなる」という条件を満たさないと、ログローテートを検知できないことに注意してください。
事前に、ログローテートを検知して漏れなく収集できるかどうか、十分に検証することを推奨します。

[^type-naming]: 慣習的にInputプラグインは`in_`というプレフィックスを付けて呼ぶことが多いですが、`@type`を指定するときはプレフィックスを付けないことが一般的です。なぜなら、`source`セクション内に設定していることでInputプラグインであることが自明になっているからです。

## カレントファイルのファイル名が変化する出力形式への対応

ログローテートの亜種として、次のようにカレントファイルのファイル名が変化する形式があります。
例えば、次のように日毎に生成された新規ファイルが、その日の間カレントファイルになる、という場合があります。

```
/tmp/test_20240306.log
/tmp/test_20240307.log <- 昨日のカレントファイル
/tmp/test_20240308.log <- 本日のカレントファイル
```

これを簡単に収集する2つの方法を紹介します。

* `strftime()`のフォーマット文字列で、当日のファイルパスを収集する
* ワイルドカードでまとめて収集する

以下それぞれの紹介です。

### `strftime()`のフォーマット文字列で、当日のファイルパスを収集する

[path](https://docs.fluentd.org/input/tail#path)設定には `%Y`, `%m`, `%d`, といった[strftime()のフォーマット文字列](https://docs.ruby-lang.org/ja/latest/method/Time/i/strftime.html)を利用できます。
例えば、`/tmp/test_%Y%m%d.log`のように設定すると、次のようにその日時に応じて動的に収集対象とするファイルパスを変更できます。

* 2024年3月7日時点: `/tmp/test_20240307.log`を収集
* 2024年3月8日時点: `/tmp/test_20240308.log`を収集

これによって、カレントファイル名が定期的に変化するようなケースに対応できます。
`in_tail`全体の設定例は次のようになります。

```xml
<source>
  @type tail
  tag test
  path /tmp/test_%Y%m%d.log
  pos_file /tmp/tail-test.pos
  <parse>
    @type none
  </parse>
</source>
```

### ワイルドカードでまとめて収集する

[path](https://docs.fluentd.org/input/tail#path)設定にはワイルドカード`*`を利用できます。
これによって、複数のファイルをまとめて収集対象とすることができます。
例えば、`/tmp/test_*.log`のように設定することで、以下のファイルをまとめて収集対象にできます。

```
/tmp/test_20240306.log
/tmp/test_20240307.log
/tmp/test_20240308.log
```

これによって、カレントファイル名が定期的に変化するようなケースにも対応できます。

しかし、今回のように前日以前のファイルにはもうログが出力されなくて、当日に相当するファイルだけ収集対象にできれば十分、というケースもあります。
その場合に、このように全部まとめて収集対象とするのはリソースの無駄使いであり、特にファイル数が多い場合はファイルハンドルなどのリソース値を圧迫する懸念があります。

そのような場合は、[limit_recently_modified](https://docs.fluentd.org/input/tail#limit_recently_modified)設定を併用して、ファイルの更新日時がある程度最近のファイルだけに収集対象を絞るとよいです。
例えば、`limit_recently_modified 3d`のように設定することで、3日以内に更新されたファイルのみに収集対象を絞り込むことができます。
（当日のファイルのみ収集すればよい場合は`1d`でもよいのですが、念のために少し余裕を持たせてもよいでしょう）。

以上に基づくと、`in_tail`全体の設定例は次のようになります。

```xml
<source>
  @type tail
  tag test
  path /tmp/test_*.log
  pos_file /tmp/tail-test.pos
  limit_recently_modified 3d
  <parse>
    @type none
  </parse>
</source>
```

## `in_tail`で収集するべきファイルであるかどうかの確認

`in_tail`は、ファイルからデータを収集するプラグインとしてFluentdに標準で組み込まれているため、ファイルからの収集によく使われます。
しかし、データソースがファイルだからといって、必ずしも`in_tail`が適切なプラグインとは限りません。

`in_tail`は、**末尾への追記**形式である**テキスト**ファイルの収集を担うプラグインである、という点に注意してください。
もしそうではないファイルの収集を行いたい場合は、適切な収集方法を広く（Fluentd以外の機能を使う選択肢も含めて）検討してください。

以下、`in_tail`が適さない主な事例です。

* バイナリファイル
* 上書き形式で更新されるファイル

## まとめ

本記事では、`in_tail`プラグインの基本的な使い方について、メンテナー視点で解説しました。

より高度な使い方をしたい場合は、[公式ドキュメント](https://docs.fluentd.org/input/tail)でどんな設定があるのかを確認してみてください。
本記事では紹介しきれなかった様々な機能を`in_tail`は持っています！

また、Fluentdコミュニティーでは、日本語用のQ&Aも用意しています。
何か疑問や困ったことがあれば、こちらで質問してみてください！

* https://github.com/fluent/fluentd/discussions/categories/q-a-japanese

最後に、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデート支援や影響のあるバグ・脆弱性のレポートなどのサポートをいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
