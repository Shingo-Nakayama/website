---
tags: []
title: Thunderbirdのインターフェースをカスタマイズするには
---
### はじめに

企業利用においては、Thunderbirdの特定の機能を使わせないようにカスタマイズしたいという場合があります。
今回はそのための機能を提供するアドオンによるカスタマイズ方法を紹介します。
<!--more-->


### globalChrome.cssとは

すべてのユーザに対してグローバルに適用するためのユーザースタイルシートを利用できるようにします。
このアドオンを使うと、特定のインターフェースの見た目を変更したり、設定を変更することのできないように非表示にしたりすることができます。

![globalChrome.cssのアドオンページ]({{ "/images/blog/20171113_0.png" | relative_url }} "globalChrome.cssのアドオンページ")

  * https://addons.mozilla.org/ja/thunderbird/addon/globalchromecss/

アドオンは上記のURLからインストール可能です。

### globalChrome.cssによるカスタマイズ例

以前[Thunderbirdのスレッドペインで既定の表示カラムを変更するには]({% post_url 2017-11-10-index %})という記事で既定のカラム表示をカスタマイズする方法を紹介しました。
今回は特定の機能を使わせないようにする例として、既定のカラム表示を変更させないようにするカスタマイズをしてみます。
そのためには、「表示する列を選択する」インターフェースの要素を特定する必要があります。

![カスタマイズ前の既定のカラム表示]({{ "/images/blog/20171113_2.png" | relative_url }} "カスタマイズ前の既定のカラム表示")

要素を特定するには、対象となる要素を開発ツールボックスを使って調査します。

![開発ツールボックスで要素を特定]({{ "/images/blog/20171113_1.png" | relative_url }} "開発ツールボックスで要素を特定")

要素が特定できたら、アドオンをインストールした環境で次のような設定を `globalChrome.css` へと反映します。

```
#threadCols > treecolpicker {
  display: none;
}
```


![カスタマイズ後の既定のカラムの状態]({{ "/images/blog/20171113_3.png" | relative_url }} "カスタマイズ後の既定のカラムの状態")

これにより、既定のカラム表示を変更できないようにすることができました。

### まとめ

今回はThunderbirdのインターフェースをカスタマイズする方法の例として、 [globalchrome.css](https://github.com/clear-code/globalchromecss) を使った例を紹介しました。

FirefoxやThunderbirdの導入やカスタマイズでお困りで、自力での解決が難しいという場合には、[有償サポート窓口](/contact/)までぜひ一度ご相談下さい。
