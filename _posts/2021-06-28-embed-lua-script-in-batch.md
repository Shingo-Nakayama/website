---
tags:
- notable-code
title: ノータブルコード17 - バッチファイルにLuaスクリプトを埋め込む技
author: kenhys
---

最近、Windowsで[LuaRocks](https://luarocks.org/)を動かす機会があった林です。

今回はLuaRocksのインストール用のバッチファイルを見ていて印象に残った、「バッチファイルにLuaスクリプトを埋め込む技」を紹介します。

<!--more-->

Windows版のLuaRocksにはスタンドアローン版とレガシー版の2種類があり、この技を使っているのはレガシー版[^legacy]のほうにありました。
LuaRocksのレガシー版では、`install.bat`を実行して指定した場所にインストールするようになっています。

[^legacy]: https://luarocks.github.io/luarocks/releases/luarocks-3.7.0-win32.zip

このバッチスクリプトの冒頭に次のような箇所がありました。

```batch
rem=rem --[[--lua
@setlocal&  set luafile="%~f0" & if exist "%~f0.bat" set luafile="%~f0.bat"
@win32\lua5.1\bin\lua5.1.exe %luafile% %*&  exit /b ]]

...(以降埋め込まれたLuaスクリプトは省略)
```

バッチファイルを実行した挙動としては、次のようになります。

* 1行目の`rem=rem --[[--lua`はバッチファイルのコメントとして無視される
* 2行目の`@setlocal&  set luafile="%~f0" & if exist "%~f0.bat" set luafile="%~f0.bat"`で`luafile`に実行中のバッチファイルのパスを設定する
* 3行目の`@win32`で始まる部分で`lua5.1.exe`の引数としてバッチファイルのパス`%luafile%`を指定して実行する
* Luaスクリプトとして実行された`rem=rem`は`rem=nil`と解釈される。続く`--[[`で3行目の終わりの`]]`までをコメントとして無視する。`--lua`は単なるコメントの文字列。
* それ以降はすべて埋め込んだLuaスクリプトを実行する
* Luaスクリプトの実行が終了したら、バッチスクリプトの実行に戻る。`exit /b`で実行したLuaスクリプトの戻り値を得る


以上、コメントとして無視される挙動をうまく活用して「バッチファイルにLuaスクリプトを埋め込む技」が実現されていることがわかります。
この手のテクニックはPolyglotなどと呼ばれているようです。他の言語の組みあわせではどうやるのかを調べてみると面白いかもしれません。
