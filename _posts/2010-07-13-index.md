---
tags:
- ruby
title: Rails 3.0 beta4でDeviseを使ってOpenID認証
---
とあるRails 3を使っている[たいやき用のCMS](http://github.com/taiyaki/taiyaking)でDeviseを使ってOpenID認証をするようにしたので、そのやり方を紹介します。Rubyは[Ruby 1.9.2 RC2](http://www.ruby-lang.org/ja/news/2010/07/12/ruby-1-9-2-rc2-is-released/)も出ていますが、今回はRuby 1.9.1を使います。
<!--more-->


### Deviseとは

[Devise](http://github.com/plataformatec/devise)は[Rack](http://rack.rubyforge.org/)ベースの認証システムです。バックエンドに[Warden](http://github.com/hassox/warden)を利用しているため、Basic認証やOpenID、OAuthなど認証方法を切り替えることができます。

ただ、以下の説明を読んでみてもらってもわかる通り、動き出すまでにそこそこの作業が必要になります。機能は豊富なので、動き出したらカスタマイズしてアプリケーションの要求に合わせていくことができるでしょう。日本語での情報もあまりありませんが、探せばいくつかはあるので、試してみてはいかがでしょうか。

とはいえ、今回はDeviseのデフォルトの認証方法ではなく、OpenIDでのみ認証することにします。また、未登録のユーザがログインしようとしたときは自動的に新規ユーザを作成することにします。このようにも使えるという例ということで読むとよいかもしれません。

### インストール

まず、Rails 3.0 beta4をインストールします。

{% raw %}
```
% sudo gem1.9.1 install rails --pre
```
{% endraw %}

サンプル用のアプリケーションを作ります。

{% raw %}
```
% ruby1.9.1 rails new taiyaki
% cd taiyaki
```
{% endraw %}

次にDeviseをインストールします。

{% raw %}
```
% sudo gem1.9.1 install devise --version=1.1.rc2
```
{% endraw %}

インストールしたDeviseを利用するため、以下のようにGemfileに追記します。

Gemfile:

{% raw %}
```ruby
gem 'devise', "1.1.rc2"
```
{% endraw %}

アプリケーションにDeviseが動作するために必要なファイルをインストールします。config/initializers/devise.rbなど主に設定ファイルです。

{% raw %}
```
% ruby1.9.1 script/rails generate devise:install
```
{% endraw %}

いくつかは手動で設定する必要があります。それぞれ以下の通りです。

Deviseはパスワードの再設定をする機能もあり、そのときはユーザにメールを送信します。そのような機能を使うときはActionMailerのURL生成オプションを設定する必要があります。例えば、開発時のホスト情報を設定する場合は以下のようになります。

config/environments/development.rb:

{% raw %}
```ruby
config.action_mailer.default_url_options = {:host => 'localhost:3000'}
```
{% endraw %}

Deviseはリダイレクト先のURLを生成するときなどにデフォルトでは`root_path`を使うので、rootパスへのマッピングを追加します。以下の例では`welcome#index`を指定しているので、後でWelcomeControllerを作ります。

config/routes.rb:

{% raw %}
```ruby
root :to => "welcome#index"
```
{% endraw %}

Deviseはnoticeとalertのflashを設定するので、レイアウトに追加しておくとよいでしょう。例えば、以下のように`yield`の前に追加します。

app/views/layouts/application.html.erb:

{% raw %}
```
<%# ... %>
<p class="notice"><%= notice %></p>
<p class="alert"><%= alert %></p>

<%= yield %>
<%# ... %>
```
{% endraw %}

### 作成

これでインストールは完了したので、コントローラーやモデルを作成します。

まず、ユーザー用のモデルを作成します。

{% raw %}
```
% ruby1.9.1 script/rails generate devise User
```
{% endraw %}

このとき生成されるスキーマは、以下のようにデータベース上にパスワードのダイジェストなどの情報を持ち、それを利用して認証することになります。

db/migrate/XXXX_devise_create_users.rb:

{% raw %}
```ruby
class DeviseCreateUsers < ActiveRecord::Migration
  def self.up
    create_table(:users) do |t|
      t.database_authenticatable :null => false
      t.recoverable
      t.rememberable
      t.trackable

      # t.confirmable
      # t.lockable :lock_strategy => :failed_attempts, :unlock_strategy => :both

      # t.token_authenticatable

      t.timestamps
    end

    add_index :users, :email,                :unique => true
    add_index :users, :reset_password_token, :unique => true
    # add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true
  end

  def self.down
    drop_table :users
  end
end
```
{% endraw %}

しかし、今回は自分では認証情報を持たずにOpenIDで認証するので、データベース上に認証情報を持たないようにします。代わりにOpenID用のカラムを追加します。

db/migrate/XXXX_devise_create_users.rb（変更後）:

{% raw %}
```ruby
class DeviseCreateUsers < ActiveRecord::Migration
  def self.up
    create_table(:users) do |t|
      t.string :email
      t.string :nickname
      t.string :identity_url
      t.string :fullname
      t.string :birth_date
      t.integer :gender
      t.string :postcode
      t.string :country
      t.string :language
      t.string :timezone

      t.rememberable
      t.trackable

      t.confirmable
      t.lockable :lock_strategy => :failed_attempts, :unlock_strategy => :both
      t.token_authenticatable

      t.timestamps
    end

    add_index :users, :identity_url,         :unique => true
    add_index :users, :email,                :unique => true
    add_index :users, :confirmation_token,   :unique => true
    add_index :users, :unlock_token,         :unique => true
  end

  def self.down
    drop_table :users
  end
end
```
{% endraw %}

変更したらスキーマを反映させます。

{% raw %}
```
% rake1.9.1 db:migrate
```
{% endraw %}

モデルのコードにもデータベースで認証するためのコードが入っています。今回は必要のないユーザ登録用の`:registerable`オプションやパスワードの入力チェックなどをする`:validatable`オプションなどは外します。`password_required?`メソッドをオーバーライドしているのは、OpenIDで認証するためパスワードが必要がないからです。

app/models/user.rb:

{% raw %}
```ruby
class User < ActiveRecord::Base
  ...
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  devise :database_authenticatable, :rememberable, :trackable
  ...

  def password_required?
    false
  end
end
```
{% endraw %}

Deviseではログイン画面などデフォルトのビューも提供してくれますが、今回はOpenIDを使った認証にするためビューをカスタマイズします。ビューをカスタマイズする場合は、コントローラーごとカスタマイズする方法と、ビューだけカスタマイズする方法がありますが、今回はコントローラーごとカスタマイズする方法にします。

コントローラーをカスタマイズするにはconfig/routes.rbに追加された`devise_for`で`:controllers`オプションを指定します。以下のように指定すると`Users::SessionsController`コントローラーを使います。

config/routes.rb:

{% raw %}
```ruby
devise_for(:users,
           :controllers => {:sessions => "users/sessions"})
```
{% endraw %}

コントローラーを作成します。

{% raw %}
```
% ruby1.9.1 script/rails generate controller Users::Sessions
```
{% endraw %}

コントローラーをカスタマイズする場合は、`ApplicationController`ではなく`Devise::SessionsController`を継承します。

app/controllers/users/sessions_controller.rb:

{% raw %}
```ruby
class Users::SessionsController < Devise::SessionsController
end
```
{% endraw %}

ログインフォームではOpenID用の識別子を入力してもらうようにします。

app/views/users/sessions/new.html.erb:

{% raw %}
```
<%= form_for(resource,
             :as => resource_name,
             :url => session_path(resource_name)) do |f| %>
  <p>
    <label for="openid_identifier" >OpenID URL:</label>
    <%= text_field_tag :openid_identifier %>
  </p>
  <p><%= f.label :remember_me %> <%= f.check_box :remember_me %></p>
  <p><%= f.submit "Login" %></p>
<% end %>
```
{% endraw %}

あとは、トップページを準備すれば画面を確認することができます。

トップページ用のコントローラーを生成します。

{% raw %}
```
% ruby1.9.1 script/rails generate controller welcome index
% rm public/index.html
```
{% endraw %}

トップページではログインページに移動できるようにします。ログイン時はログイン中のユーザ情報を表示します。

app/views/welcome/index.html.erb:

{% raw %}
```
<h1>Welcome#index</h1>

<% if user_signed_in? %>
  <p>ようこそ<%= current_user.nickname %>さん</p>
  <%= link_to("ログアウト", destroy_user_session_path) %>
<% else %>
  <%= link_to("ログイン", new_user_session_path) %>
<% end %>
```
{% endraw %}

サーバを起動します。

{% raw %}
```
% ruby1.9.1 script/rails server
```
{% endraw %}

http://localhost:3000/にアクセスすると以下のような画面になります。

![トップページ]({{ "/images/blog/20100713_1.png" | relative_url }} "トップページ")

ログインページに行くと以下のようなフォームになります。

![ログインフォーム]({{ "/images/blog/20100713_0.png" | relative_url }} "ログインフォーム")

### OpenID対応

それではOpenIDに対応します。DeviseからOpenIDを使うために、[warden-openid](http://github.com/ursm/warden-openid)を使います。

{% raw %}
```
% sudo gem1.9.1 install warden-openid
```
{% endraw %}

Gemfileにも追記します。

Gemfile:

{% raw %}
```ruby
gem 'warden-openid'
```
{% endraw %}

WargenでOpenIDを使うようにします。

config/initializers/devise.rb:

{% raw %}
```ruby
Devise.setup do |config|
  ...
  config.warden do |manager|
    manager.default_strategies(:openid, :scope => :user)
  end
end
```
{% endraw %}

OpenIDの設定をします。warden-openidではOpenIDの認証が成功した時にコールバックが実行され、そこで認証情報に対応したアプリケーション用のユーザを返すことになります。今回は、ここで、ユーザが存在しない場合は自動的に新規ユーザを作成することにします。

config/initializers/openid.rb:

{% raw %}
```ruby
Rails.application.config.middleware.insert(Warden::Manager, Rack::OpenID)

Warden::OpenID.configure do |config|
  config.required_fields = User.required_open_id_fields
  config.optional_fields = User.optional_open_id_fields
  config.user_finder do |response|
    user = User.find_by_identity_url(response.identity_url)
    if user.nil?
      user = User.new
      user.extract_open_id_values(response)
      unless user.save
        message = "failed to create user: "
        message << "#{users.errors.full_messages.inspect}: "
        message << user.inspect
        Rails.logger.error(message)
        user = nil
      end
    end
    user
  end
end
```
{% endraw %}

OpenIDの情報とアプリケーションのユーザ情報をマッピングする処理はモデルで行います。

app/models/user.rb:

{% raw %}
```ruby
class User < ActiveRecord::Base
  REQUIRED_FIELDS = {
    :nickname => "nickname",
  }

  OPTIONAL_FIELDS = {
    :email => "email",
    :fullname => "fullname",
    :birth_date => "dob",
    :gender => "gender",
    :postcode => "postcode",
    :country => "country",
    :language => "language",
    :timezone => "timezone"
  }

  class << self
    def required_open_id_fields
      REQUIRED_FIELDS.values
    end

    def optional_open_id_fields
      OPTIONAL_FIELDS.values
    end
  end

  def password_required?
    false
  end

  def extract_open_id_values(response)
    profile_data = {}
    [OpenID::SReg::Response, OpenID::AX::FetchResponse].each do |response_class|
      data_response = response_class.from_success_response(response)
      profile_data.merge!(data_response.data) if data_response
    end
    [REQUIRED_FIELDS, OPTIONAL_FIELDS].each do |fields|
      fields.each do |model_key, profile_key|
        unless profile_data[profile_key].blank?
          self.send("#{model_key}=", profile_data[profile_key])
        end
      end
    end
    self.identity_url = response.identity_url
    self.nickname ||= identity_url
  end
end
```
{% endraw %}

一応、ニックネーム情報は欲しいとリクエストしますが、もらえなくてもなんとなく動くようになっています。この状態でログインページにOpenID識別子を入力して、認証に成功するとアプリケーションにログインする事ができます。

![ログイン成功]({{ "/images/blog/20100713_2.png" | relative_url }} "ログイン成功")

ただし、現在リリースされている[ruby-openid](http://github.com/openid/ruby-openid)はRuby 1.9のEncodingに対応していないため、認証中にASCII以外の文字列を含むページにアクセスすることになると失敗します。これを修正する方法は[Ruby 1.9.1 support](http://github.com/openid/ruby-openid/issues#issue/2)で報告済みですが、まだ取り込まれていません。

### まとめ

Rails 3.0 beta4でDeviseを使ってOpenID認証する方法を紹介しました。Rails 3で認証まわりはどうしようか、と考えていている人は試してみるとよいかもしれません。ただ、betaやrcのものを使っているので、これから使い方は変わっていく可能性が高いと考えられます。注意してください。

そういえば、[トップページ](/)にある[会社紹介資料PDF](/clear-code.pdf)を更新しました。エンジニア紹介ページなどが更新されています。
